@extends('layouts.app')

@section('logged')



    <div class="customize_card_outer">
        <div class="tabs_container">

            <div class="container">

                <div class="tab-wrap">

                    <!-- active tab on page load gets checked attribute -->
                    <input type="radio" id="tab1" name="tabGroup1" value="limited" class="tab">
                    <label for="tab1">Limited Voucher</label>

                    <input type="radio" id="tab2" name="tabGroup1" value="normal" class="tab">
                    <label for="tab2">Normal Voucher</label>

                    <div class="tab__content" style="display:flex; flex-wrap:wrap">
                        @if(isset($voucher_list_limited))
                            @foreach($voucher_list_limited as $type)
                                @if(!empty($type))
                                <div class="customize_card_wrapper">

                                    <div class="customize_card_left">
                                        <img src="{{$type->image}}" width="100%" height="100%" alt="">
                                    </div>

                                    <div class="customize_card_right">
                                        <div class="customize_card_right_content">
                                            <div class="bg-dark v_title"><span>{{$type->type_name}}</span></div>


                                            @if($type->type == 'limited')

                                                    @if(($type->publish_date <= date('Y-m-d H:i:s')) && ($type->unpublish_date >= date('Y-m-d H:i:s')) && !(Auth::user()->checkHaveLimitedVoucher($type->id)) && (Auth::user()->CheckHaveLimitedVoucherAvailable($type->id)))
                                                        <p>Available</p>
                                                        <form method="POST" id="get_voucher_form" action="{{ route('get-voucher') }}">
                                                            @csrf
                                                            @method('GET')
                                                            <input type="hidden" name="voucher_type" value="limited">
                                                            <input type="hidden" name="voucher_type_id" value="{{$type->id}}">

                                                            @if(!Auth::user()->CheckHaveLimitedVoucherAvailable($type->id))
                                                            <button type="button" class="btn_getv btn btn-outline-secondary" disable style="width:100% ;cursor: not-allowed;"> <i class="fa fa-ban mr-2" aria-hidden="true"></i>Fully Redeem</button>
                                                            @else
                                                            <button index-btn="{{$loop->index}}" btn-type="l" id="btn_submit_getv_l_{{$loop->index}}" type="submit" class="btn_submit_getv btn_getv btn btn-outline-primary" style="width:100%">
                                                                <i id="btn_submit_getv_text_l_{{$loop->index}}" class="fa fa-check-square-o mr-2" aria-hidden="true"></i>Get Voucher
                                                            </button>
                                                            @endif

                                                        </form>

                                                    @elseif($type->publish_date > date('Y-m-d H:i:s'))
                                                        <p>Coming soon</p>
                                                        <button type="button" class="btn_getv btn btn-outline-secondary" disable style="width:100% ;cursor: not-allowed;"> <i class="fa fa-ban mr-2" aria-hidden="true"></i>Not available</button>
                                                    @elseif(!Auth::user()->CheckHaveLimitedVoucherAvailable($type->id))
                                                        <p>Fully Redeem</p>
                                                        <button type="button" class="btn_getv btn btn-outline-secondary" disable style="width:100% ;cursor: not-allowed;"> <i class="fa fa-ban mr-2" aria-hidden="true"></i>Not available</button>
                                                    @else
                                                        <p>Not Available</p>
                                                        <button type="button" class="btn_getv btn btn-outline-secondary" disable style="width:100% ;cursor: not-allowed;"> <i class="fa fa-ban mr-2" aria-hidden="true"></i>Not available</button>
                                                    @endif

                                            @endif

                                            @if($type->type == 'normal')
                                                    @if(!(Auth::user()->checkHaveNormalVoucher($type->id)) && (Auth::user()->checkHaveNormalVoucherAvailable($type->id)))
                                                        <p>Available</p>
                                                        <form method="POST" id="get_voucher_form" action="{{ route('get-voucher') }}">
                                                            @csrf
                                                            @method('GET')
                                                            <input type="hidden" name="voucher_type" value="normal">
                                                            <input type="hidden" name="voucher_type_id" value="{{$type->id}}">

                                                            @if(!Auth::user()->checkHaveNormalVoucherAvailable($type->id))
                                                            <button type="button" class="btn_getv btn btn-outline-secondary" disable style="width:100% ;cursor: not-allowed;"> <i class="fa fa-ban mr-2" aria-hidden="true"></i>Fully Redeem</button>
                                                            @else
                                                            <button index-btn="{{$loop->index}}" btn-type="n" id="btn_submit_getv_n_{{$loop->index}}" type="submit" class="btn_submit_getv btn_getv btn btn-outline-primary" style="width:100%">
                                                                <i id="btn_submit_getv_text_n_{{$loop->index}}" class="fa fa-check-square-o mr-2" aria-hidden="true"></i>Get Voucher
                                                            </button>
                                                            @endif
                                                        </form>

                                                    @elseif(!Auth::user()->checkHaveNormalVoucherAvailable($type->id))
                                                        <p>Fully Redeem</p>
                                                        <button type="button" class="btn_getv btn btn-outline-secondary" disable style="width:100% ;cursor: not-allowed;"> <i class="fa fa-ban mr-2" aria-hidden="true"></i>Not available</button>
                                                    @else
                                                        <p>Not Available</p>
                                                        <button type="button" class="btn_getv btn btn-outline-secondary" disable style="width:100% ;cursor: not-allowed;"> <i class="fa fa-ban mr-2" aria-hidden="true"></i>Not available</button>
                                                    @endif
                                            @endif

                                            <div></div>


                                        </div>
                                    </div>

                                </div>
                                @endif
                            @endforeach
                        @endif


                        @if(isset($voucher_list_limited))
                            @if($voucher_list_limited->count() == 0)
                                <h6>No Voucher..</h6>
                            @endif
                        @endif

                        <div style="width:100%; display: flex; justify-content: end;">
                        {{$voucher_list_limited->appends(['voucher-list-limited' => $voucher_list_limited->currentPage()])->links()}}
                        </div>
                    </div>
                    <div class="tab__content" style="display:flex; flex-wrap:wrap">
                        @if(isset($voucher_list_normal))
                            @foreach($voucher_list_normal as $type)
                                @if(!empty($type))
                                <div class="customize_card_wrapper">

                                    <div class="customize_card_left">
                                        <img src="{{$type->image}}" width="100%" height="100%" alt="">
                                    </div>

                                    <div class="customize_card_right">
                                        <div class="customize_card_right_content">
                                            <div class="bg-dark v_title"><span>{{$type->type_name}}</span></div>


                                            @if($type->type == 'limited')

                                                    @if($type->status == 'available')
                                                        <p>Available</p>
                                                        <form method="POST" id="get_voucher_form" action="{{ route('get-voucher') }}">
                                                            @csrf
                                                            @method('GET')
                                                            <input type="hidden" name="voucher_type" value="limited">
                                                            <input type="hidden" name="voucher_type_id" value="{{$type->id}}">

                                                            @if(!Auth::user()->CheckHaveLimitedVoucherAvailable($type->id))
                                                            <button type="button" class="btn_getv btn btn-outline-secondary" disable style="width:100% ;cursor: not-allowed;"> <i class="fa fa-ban mr-2" aria-hidden="true"></i>Fully Redeem</button>
                                                            @else
                                                            <button index-btn="{{$loop->index}}" btn-type="l" id="btn_submit_getv_l_{{$loop->index}}" type="submit" class="btn_submit_getv btn_getv btn btn-outline-primary" style="width:100%">
                                                                <i id="btn_submit_getv_text_l_{{$loop->index}}" class="fa fa-check-square-o mr-2" aria-hidden="true"></i>Get Voucher
                                                            </button>
                                                            @endif

                                                        </form>

                                                    @elseif($type->status == 'coming soon')
                                                        <p>Coming soon</p>
                                                        <button type="button" class="btn_getv btn btn-outline-secondary" disable style="width:100% ;cursor: not-allowed;"> <i class="fa fa-ban mr-2" aria-hidden="true"></i>Not available</button>
                                                    @elseif($type->status == 'fully redeem')
                                                        <p>Fully Redeem</p>
                                                        <button type="button" class="btn_getv btn btn-outline-secondary" disable style="width:100% ;cursor: not-allowed;"> <i class="fa fa-ban mr-2" aria-hidden="true"></i>Not available</button>
                                                    @else
                                                        <p>Not Available</p>
                                                        <button type="button" class="btn_getv btn btn-outline-secondary" disable style="width:100% ;cursor: not-allowed;"> <i class="fa fa-ban mr-2" aria-hidden="true"></i>Not available</button>
                                                    @endif

                                            @endif

                                            @if($type->type == 'normal')
                                                    @if($type->status == 'available')
                                                        <p>Available</p>
                                                        <form method="POST" id="get_voucher_form" action="{{ route('get-voucher') }}">
                                                            @csrf
                                                            @method('GET')
                                                            <input type="hidden" name="voucher_type" value="normal">
                                                            <input type="hidden" name="voucher_type_id" value="{{$type->id}}">

                                                            @if(!Auth::user()->checkHaveNormalVoucherAvailable($type->id))
                                                            <button type="button" class="btn_getv btn btn-outline-secondary" disable style="width:100% ;cursor: not-allowed;"> <i class="fa fa-ban mr-2" aria-hidden="true"></i>Fully Redeem</button>
                                                            @else
                                                            <button index-btn="{{$loop->index}}" btn-type="n" id="btn_submit_getv_n_{{$loop->index}}" type="submit" class="btn_submit_getv btn_getv btn btn-outline-primary" style="width:100%">
                                                                <i id="btn_submit_getv_text_n_{{$loop->index}}" class="fa fa-check-square-o mr-2" aria-hidden="true"></i>Get Voucher
                                                            </button>
                                                            @endif
                                                        </form>

                                                    @elseif($type->status == 'fully redeem')
                                                        <p>Fully Redeem</p>
                                                        <button type="button" class="btn_getv btn btn-outline-secondary" disable style="width:100% ;cursor: not-allowed;"> <i class="fa fa-ban mr-2" aria-hidden="true"></i>Not available</button>
                                                    @else
                                                        <p>Not Available</p>
                                                        <button type="button" class="btn_getv btn btn-outline-secondary" disable style="width:100% ;cursor: not-allowed;"> <i class="fa fa-ban mr-2" aria-hidden="true"></i>Not available</button>
                                                    @endif
                                            @endif

                                            <div></div>


                                        </div>
                                    </div>

                                    </div>
                                @endif
                            @endforeach
                        @endif


                        @if(isset($voucher_list_normal))
                            @if($voucher_list_normal->count() == 0)
                                <h6>No Voucher..</h6>
                            @endif
                        @endif

                        <div style="width:100%; display: flex; justify-content: end;">
                        {{$voucher_list_normal->appends(['voucher-list-normal' => $voucher_list_normal->currentPage()])->links()}}
                        </div>
                    </div>


                </div>

            </div>
        </div>

    </div>

@endsection

@section('scripts')

<script>
    $(".get_voucher_form").click(function(e) {

       e.preventDefault();
       let ind =this.getAttribute('index-btn');
       let btn_type =this.getAttribute('btn-type');

       if(btn_type == 'l'){
        $("#btn_submit_getv_text_l_"+ind).removeClass('fa fa-check-square-o');
        $("#btn_submit_getv_text_l_"+ind).addClass('spinner-border spinner-border-sm');
        $('#btn_submit_getv_l_'+ind).attr('disabled','disabled');
       }else{
        $("#btn_submit_getv_text_n_"+ind).removeClass('fa fa-check-square-o');
        $("#btn_submit_getv_text_n_"+ind).addClass('spinner-border spinner-border-sm');
        $('#btn_submit_getv_n_'+ind).attr('disabled','disabled');
       }

       $("#get_voucher_form").submit();
    });


    $('input[type=radio][name=tabGroup1]').change(function() {
        if (this.value == 'limited') {
            if (typeof(Storage) !== "undefined") {
                localStorage.setItem("tabs", "home_limited");
            }
        }
        else if (this.value == 'normal') {
            if (typeof(Storage) !== "undefined") {
                localStorage.setItem("tabs", "home_normal");
            }
        }
    });
    if (typeof(Storage) !== "undefined") {

        let currentTab = localStorage.getItem("tabs");

        if(currentTab == null) $("#tab1").prop("checked", true);

        if(currentTab == 'home_limited') $("#tab1").prop("checked", true);
        else if(currentTab == 'home_normal') $("#tab2").prop("checked", true);

    }


</script>

@endsection




