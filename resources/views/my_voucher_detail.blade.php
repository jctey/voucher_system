@extends('layouts.app')

@section('logged')

<div class="container">

    <div class="customize_card_outer_detail">

        @if(isset($details))

            @if($details->used == false)

                @if($voucher_type == 'limited')
                <div style="background-image:url('{{ isset($details->image) ? $details->image : ''}}')"
                    class="backg {{$voucher_type == 'limited'? 'card_detail_blue':'card_detail'}}">
                @else
                <div style="background-image:url('{{ isset($details->image) ? $details->image : ''}}')"
                    class="backg card_detail">
                @endif

                    <div class="customize_card_content">
                        @if(isset($voucher_type))

                            @if($voucher_type == 'newuser')
                            <h4>{{ $details->type_name }}</h4>
                            <div style="border: 1px solid grey">{!! $details->type_description !!}</div>
                            @elseif($voucher_type == 'birthday')
                            <h4>{{ $details->type_name }}</h4>
                            <div style="border: 1px solid grey">{!! $details->type_description!!}</div>
                            @elseif($voucher_type == 'limited')
                            <h4>{{$details->voucherLimitedType->type_name}}</h4>
                            <div style="border: 1px solid grey">{!! $details->voucherLimitedType->description !!}</div>
                            @elseif($voucher_type == 'normal')
                            <h4>{{$details->voucherNormalType->type_name}}</h4>
                            <div style="border: 1px solid grey">{!! $details->voucherNormalType->description !!}</div>
                            @endif

                        @endif

                        <div id="cd_text"></div>

                        <div id="vcode_wrapper">

                        </div>


                        <div>Created at :{{$details->created_at}}</div>
                        <div class="mt-2" style="text-align:center">
                            @if($details->status == 'active')
                            <button type="submit" id="btn_active2" class="btn btn-outline-primary" disabled>
                            <span id="btn_active_text2" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                Activated
                            </button>
                            @else
                            <button type="submit" id="btn_active" class="btn btn-outline-dark">
                            <span id="btn_active_text" class="" role="status" aria-hidden="true"></span>
                                Active
                            </button>
                            @endif

                        </div>


                    </div>



                </div>
            @endif
        @endif

    </div>

</div>
@endsection

@section('scripts')
<script>

    $(document).ready(function() {

        let countdown = "{{$time_left}}";
        let minute ;
        let second ;

        if(countdown == 'Invalid date'){

        }else if (countdown.indexOf(':') !== -1) {
            countDownStart();
        }

        function countDownStart(){
            var fields = countdown.split(':');

            if(fields.length == 2){
                minute = fields[0];
                second = fields[1];
            }

            let itv =setInterval(function () {
               document.getElementById("cd_text").innerHTML =
               pad(minute) + " : " + pad(second);

               $('#vcode_wrapper').html(
                   '<div>Voucher code :<div><strong>{{$details->voucher_code}}</strong></div></div>'
               )

               $('#cd_text').addClass('cd_text');
                  second--;
               if (second == 00) {
                  minute--;

                  if ((minute == -1) && (second == 0)) {
                    clearInterval(itv);
                    document.getElementById("cd_text").innerHTML = 'Times up';
                    $('#btn_active_text2').removeClass('spinner-border spinner-border-sm');
                  }
                  second = 60;
               }
            }, 1000);
        }

        $("#btn_active").click(()=>{

            $("#btn_active_text").addClass('spinner-border spinner-border-sm');
            $('#btn_active').attr('disabled','disabled');

            $.ajax({
                type:'PUT',
                url:'{{route("activate_voucher")}}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "voucher_type": "{{$voucher_type}}",
                    "type_id": "{{$type_id}}"
                },
                success:function(data) {
                    if(data.msg == 'success'){
                        minute =4;
                        second =59;
                        countDownStart();
                    }
                }
            });

        })

        function pad(d) {
            return (d < 10) ? '0' + d.toString() : d.toString();
        }
    });


</script>
@endsection


