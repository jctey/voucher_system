@extends('layouts.app')

@section('logged')

<div class="container">

    <div class="customize_card_outer">
        @if(isset($vouchers))

            @foreach($vouchers as $voucher)

                @if($voucher->show == 1 || $voucher->show == 2)
                    @if($voucher->type == 'birthday')

                        @if(auth()->user()->ShowBeforeOneWeek())
                        <div class="customize_card_wrapper">

                        <div class="customize_card_left">
                            <img src="{{$voucher->image}}" width="100%" height="100%" alt="">
                        </div>
                        <div class="customize_card_right">
                            <div class="customize_card_right_content">
                                <div class="bg-dark v_title"><span><strong>{{$voucher->voucher_type_id}}</strong></span></div>
                                <div style="display: inline-flex; justify-content: space-around;">Voucher Status :<div class="{{$voucher->status == 'active'? 'status_success':'status_inactive'}}"> {{$voucher->status == 'active'? 'active' :'unused'}}</div></div>
                                <form method="POST" style="display:contents" action="{{ route('my-voucher-detail') }}">
                                    @csrf
                                    @method('GET')
                                    <input type="hidden" name="type" value="{{$voucher->type}}">
                                    <input type="hidden" name="type_id" value="{{$voucher->id}}">

                                    @if(date('d-m', strtotime(auth()->user()->date_of_birth)) == date('d-m'))
                                    <button type="submit" class="btn btn-outline-dark"> <i class="fa fa-check-square-o" aria-hidden="true"></i> Check Detail</button>
                                    @else
                                    <button type="button" class="btn btn-outline-dark" disable style="width:100% ;cursor: not-allowed;"> <i class="fa fa-check-square-o" aria-hidden="true"></i> Check Detail</button>
                                    @endif
                                </form>
                            </div>
                        </div>

                        </div>
                        @endif

                    @else
                    <div class="{{$voucher->type == 'limited'? 'customize_card_wrapper_shadowbox_blue':'customize_card_wrapper'}}">

                    <div class="customize_card_left">
                        <img src="{{$voucher->image}}" width="100%" height="100%" alt="">
                    </div>
                    <div class="customize_card_right">
                        <div class="customize_card_right_content">
                            <div class="bg-dark v_title"><span><strong>{{$voucher->voucher_type_id}}</strong></span></div>
                            <div style="display: inline-flex; justify-content: space-around;">Voucher Status :<div class="{{$voucher->status == 'active'? 'status_success':'status_inactive'}}"> {{$voucher->status == 'active'? 'active' :'unused'}}</div></div>
                            <form method="POST" style="display:contents" action="{{ route('my-voucher-detail') }}">
                                @csrf
                                @method('GET')
                                <input type="hidden" name="type" value="{{$voucher->type}}">
                                <input type="hidden" name="type_id" value="{{$voucher->id}}">
                                <button type="submit" class="btn btn-outline-dark"> <i class="fa fa-check-square-o" aria-hidden="true"></i> Check Detail</button>
                            </form>
                        </div>
                    </div>

                    </div>
                    @endif
                @endif

            @endforeach
        @endif

        @if(isset($vouchers))
            @if(count($vouchers) == 0)
                <p>You have no voucher...</p>
            @endif
        @endif

        <div class="paginate_customize">{{$vouchers->links()}}</div>
    </div>

</div>
@endsection
