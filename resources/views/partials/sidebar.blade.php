<nav id="sidebar" class="sidebar js-sidebar">
  <div class="sidebar-content js-simplebar">
    <a class="sidebar-brand" href="index.html">
      <span class="align-middle">{{ config('app.name', 'Laravel') }}</span>
    </a>
    @if(auth()->user()->is_admin)
    <!-- admin -->
    <ul class="sidebar-nav">
      <li class="sidebar-header">
        Pages
      </li>
          @php
            $currentRouteName = Route::currentRouteName();
          @endphp


      <li class="sidebar-item {{ $currentRouteName == 'dashboard' ? 'active' : ''  }}">
        <a class="sidebar-link" href="{{ route('dashboard') }}">
          <i class="align-middle" data-feather="globe"></i> <span class="align-middle">Dashboard</span>
        </a>
      </li>
      <li class="sidebar-item {{ $currentRouteName == 'user-index' ? 'active' : ''  }}">
        <a class="sidebar-link" href="{{ route('user-index') }}">
          <i class="align-middle" data-feather="user-check"></i> <span class="align-middle">User List</span>
        </a>
      </li>

      <li class="sidebar-item {{ $currentRouteName == 'voucher-newuser-index' ? 'active' : ''  }}">
        <a class="sidebar-link" href="{{ route('voucher-newuser-index') }}">
          <i class="align-middle" data-feather="grid"></i> <span class="align-middle">Voucher Registered</span>
        </a>
      </li>

      <li class="sidebar-item {{ $currentRouteName == 'voucher-birthday-index' ? 'active' : ''  }}">
        <a class="sidebar-link" href="{{ route('voucher-birthday-index') }}">
          <i class="align-middle" data-feather="layout"></i> <span class="align-middle">Voucher Birthday</span>
        </a>
      </li>

      <li class="sidebar-item {{ $currentRouteName == 'voucher-limited-index' ? 'active' : ''  }}">
        <a class="sidebar-link" href="{{ route('voucher-limited-index') }}">
          <i class="align-middle" data-feather="menu"></i> <span class="align-middle">Voucher Limited</span>
        </a>
      </li>

      <li class="sidebar-item {{ $currentRouteName == 'voucher-normal-index' ? 'active' : ''  }}">
        <a class="sidebar-link" href="{{ route('voucher-normal-index') }}">
          <i class="align-middle" data-feather="layers"></i> <span class="align-middle">Voucher Normal</span>
        </a>
      </li>

      <li class="sidebar-item">
        <a class="sidebar-link" href="{{ route('logout') }}" id="sidebar_logout">
          <i class="align-middle" data-feather="log-out"></i> <span class="align-middle">Log Out</span>
        </a>

        <form id="sidebar-logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
      </li>
    </ul>
    @else
    <!-- frontend -->
    <ul class="sidebar-nav">
      <li class="sidebar-item">
        <a class="sidebar-link" href="{{route('home')}}">
          <i class="align-middle" data-feather="home"></i> <span class="align-middle">Home</span>
        </a>
      </li>

      <li class="sidebar-item">
        <a class="sidebar-link" href="{{route('my-voucher')}}">
          <i class="align-middle" data-feather="bookmark"></i> <span class="align-middle">My Voucher</span>
        </a>
      </li>

      <li class="sidebar-item">
        <a class="sidebar-link" href="{{route('history')}}">
          <i class="align-middle" data-feather="clipboard"></i> <span class="align-middle">History</span>
        </a>
      </li>

      <li class="sidebar-item">
        <a class="sidebar-link" href="{{ route('logout') }}" id="sidebar_logout">
          <i class="align-middle" data-feather="log-out"></i> <span class="align-middle">Log Out</span>
        </a>

        <form id="sidebar-logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
      </li>
    </ul>

    @endif


  </div>
</nav>

@section('scripts')
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
  $(".sidebar-item").on('click',function(e){
      e.preventDefault();

      $('.sidebar-nav').load($(this).attr('href'));
      $(this).addClass("active");  // adding active class
  });
</script> -->
@endsection