<!-- Modal -->
<div class="modal fade" id="clientVoucherModal" tabindex="-1" role="dialog" aria-labelledby="clientVoucherModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Detail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

        <div class="modal-body" style="display:flex; flex-direction:column">
              <div>
                <div style="display:inline-flex; width:100%">
                  <label class="col-5">Limited voucher: </label>
                  <div class="radio col-3">
                    <label style="display:flex; align-items:center">
                      <input type="radio" style="margin-right: 5px" name="limited_used" value="l-used">Used
                    </label>
                  </div>
                  <div class="radio col-3">
                    <label style="display:flex; align-items:center">
                      <input type="radio" style="margin-right: 5px" name="limited_used" value="l-unused">Unused
                    </label>
                  </div>
                </div>

              </div>
        </div>
        <div class="modal-footer">
          <button type="submit" id="l_filter_btn" class="btn btn-primary" data-dismiss="modal">Active</button>
        </div>

    </div>
  </div>
</div>
