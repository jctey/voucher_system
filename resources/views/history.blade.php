@extends('layouts.app')

@section('logged')

<div class="container">

    <div class="customize_card_outer_history">

        @if(isset($histories))
            @if(count($histories)>0)
                @foreach($histories as $history)
                    @if($history->show == 1 || $history->show == 2 || $history->show == 3)

                    <div class="customize_card_wrapper">

                        <div class="customize_card_left">
                            <img src="{{$history->image}}" width="100%" height="100%" alt="">
                        </div>
                        <div class="customize_card_right">
                            <div class="customize_card_right_content">
                                <div class="bg-dark v_title"><span><strong>{{$history->voucher_type_id}}</strong></span></div>
                                <div class='customize_card_left_history' >
                                    <div style="display:flex; align-items: center">Voucher Code :<div class="ml-2"><strong>{{$history->voucher_code}}</strong></div></div>
                                    <div style="display:flex; align-items: center">Used at :<div class="ml-2"><strong>{{$history->updated_at}}</strong></div></div>
                                </div>

                            </div>
                        </div>

                    </div>

                    @endif
                @endforeach
            @endif

            @if(count($histories) == 0)
            <div class="history_content">
                <div>No record...</div>
            </div>
            @endif

            <div class="paginate_customize">{{$histories->links()}}</div>
        @endif

    </div>

</div>
@endsection