@extends('layouts.app')

@section('admin-logged')

<div class="card-header" style="display:flex; justify-content:space-between">
    <div style="align-self: center;">
        <span>Voucher Birthday Type</span>
    </div>
    <a
            class="btn btn-outline-primary"
            style="box-shadow: 2px 3px 6px 4px #dddddd;"
            href="{{ route('voucher-birthday-type-create') }}"
    >
        Create New Type
    </a>
</div>

<div class="card-body" style="overflow-x:auto">
    <div class="panel-body">
        <table class="table table-bordered" id="birthday-type-table" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Type name</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div class="card-header" style="display:flex; justify-content:space-between">
    <div style="align-self: center; width:100%; display:contents">
        <span>Voucher Birthday list</span>

        <select id="birthday_type" class="custom-select custom-select-sm col-6">
            @foreach($birthday_type as $type)
                <option value="{{$type->id}}">{{$type->type_name}}</option>
            @endforeach
        </select>

    </div>

    <button
        type="button"
        class="btn btn-outline-primary"
        data-toggle="modal"
        data-target="#birthdayFilterModalCenter"
    >
        <i class="fa fa-filter" aria-hidden="true"></i>
    </button>
</div>

<div class="card-body" style="overflow-x:auto">
    <div class="panel-body">
        <table class="table table-bordered" id="birthday-table" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Voucher Code</th>
                    <th>Owned by</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>

    <!-- modal -->
    @include('partials.modal.vbirthday_modal')
    @include('partials.modal.confirm_modal')

</div>
@endsection

@section('head')
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.1.2/handlebars.min.js"></script>

<script>

        var tableType = $('#birthday-type-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: true,
            fixedColumns:   {
                heightMatch: '50px'
            },
            ajax: {
                url:'{{ route('voucher-birthday-type-list-datatable') }}',
                data: function (d) {

                }
            },

            columns: [
                { data: 'DT_RowIndex', name: 'index' },
                { data: 'type_name', name: 'type_name' },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
            ],
            order: [[2, 'asc']]
        });

        var table = $('#birthday-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: true,
            ajax: {
                url:'{{ route('voucher-birthday-list-datatable') }}',
                data: function (d) {
                    d.birthday_used = $('input[name="birthday_used"]:checked').val();
                    d.birthday_verify = $('input[name="birthday_verify"]:checked').val();
                    d.birthday_type = $('#birthday_type').val();
                }
            },

            columns: [
                { data: 'DT_RowIndex', name: 'index' },
                { data: 'voucher_code', name: 'voucher_code' },
                { data: 'user_id', name: 'user_id' },
                { data: 'used', name: 'used' },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
            ],
            order: [[3, 'asc']]
        });

        $(document).ready(function () {

            $("#birthday_filter_btn").click(function(e) {
                table.ajax.reload();
            });

            $("#birthday_type").change(()=>{
                table.ajax.reload();
            });

            $("#birthday_filter_reset_btn").click(function(e) {

                $('input[name="birthday_used"]:checked').prop('checked', false);
                $('input[name="birthday_verify"]:checked').prop('checked', false);

                table.ajax.reload();
            });

            $('#confirmModal').on('shown.bs.modal', function(e) {

                //get data-id attribute of the clicked element

                let type = $(e.relatedTarget).data('modaltype');

                if(type == 'birthday'){

                    let user_id = $(e.relatedTarget).data('userid');

                    let url = '{{ route("voucher-birthday-delete", ":id") }}';
                    url = url.replace(':id', user_id);
                    $('#confirm_delete_form').attr('action', url);

                }else if(type == 'birthday_type'){

                    let type_id = $(e.relatedTarget).data('typeid');

                    let url = '{{ route("voucher-birthday-type-delete", ":id") }}';
                    url = url.replace(':id', type_id);
                    $('#confirm_delete_form').attr('action', url);
                }else return false


            });


        });



</script>


@endsection


