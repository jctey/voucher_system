@extends('layouts.app')

@section('admin-logged')

<div class="card-header" style="display:flex; justify-content:space-between">
    <div style="align-self: center;">
        <span>Voucher Limited Type</span>
    </div>
    <a
            class="btn btn-outline-primary"
            style="box-shadow: 2px 3px 6px 4px #dddddd;"
            href="{{ route('voucher-limited-type-create') }}"
    >
        Create New Type
    </a>
</div>

<div class="card-body" style="overflow-x:auto">
    <div class="panel-body">
        <table class="table table-bordered" id="limited-type-table" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Type name</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div class="card-header" style="display:flex; justify-content:space-between">
    <div style="align-self: center; width:100%; display:contents">
        <span>Voucher Limited list</span>

        <select id="limited_type" class="custom-select custom-select-sm col-6">
            @foreach($limited_type as $type)
                <option value="{{$type->id}}">{{$type->type_name}}</option>
            @endforeach
        </select>

    </div>

    <button
        type="button"
        class="btn btn-outline-primary"
        data-toggle="modal"
        data-target="#limitedFilterModalCenter"
    >
        <i class="fa fa-filter" aria-hidden="true"></i>
    </button>
</div>

<div class="card-body" style="overflow-x:auto">
    <div class="panel-body">
        <table class="table table-bordered" id="limited-table" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Voucher Code</th>
                    <th>Owned by</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>

    <!-- modal -->
    @include('partials.modal.vlimited_modal')
    @include('partials.modal.confirm_modal')

</div>
@endsection

@section('head')
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- <link href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css" rel="stylesheet"> -->
@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<!-- <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.1.2/handlebars.min.js"></script>



<script>

        var tableType = $('#limited-type-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: true,
            fixedColumns:   {
                heightMatch: '50px'
            },
            ajax: {
                url:'{{ route('voucher-limited-type-list-datatable') }}',
                data: function (d) {
                    // d.limited_used = $('input[name="limited_used"]:checked').val();
                    // d.limited_verify = $('input[name="limited_verify"]:checked').val();
                    // d.limited_type = $('#limited_type').val();
                }
            },

            columns: [
                { data: 'DT_RowIndex', name: 'index' },
                { data: 'type_name', name: 'type_name' },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
            ],
            order: [[2, 'asc']]
        });

        var table = $('#limited-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: true,
            ajax: {
                url:'{{ route('voucher-limited-list-datatable') }}',
                data: function (d) {
                    d.limited_used = $('input[name="limited_used"]:checked').val();
                    d.limited_verify = $('input[name="limited_verify"]:checked').val();
                    d.limited_type = $('#limited_type').val();
                }
            },

            columns: [
                { data: 'DT_RowIndex', name: 'index' },
                { data: 'voucher_code', name: 'voucher_code' },
                { data: 'user_id', name: 'user_id' },
                { data: 'used', name: 'used' },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
            ],
            order: [[3, 'asc']]
        });

        $(document).ready(function () {

            $("#l_filter_btn").click(function(e) {
                table.ajax.reload();
            });

            $("#limited_type").change(()=>{
                table.ajax.reload();
            });

            $("#l_filter_reset_btn").click(function(e) {

                $('input[name="limited_used"]:checked').prop('checked', false);
                $('input[name="limited_verify"]:checked').prop('checked', false);

                table.ajax.reload();
            });

            $('#confirmModal').on('shown.bs.modal', function(e) {

                //get data-id attribute of the clicked element

                let type = $(e.relatedTarget).data('modaltype');

                if(type == 'limited'){

                    let user_id = $(e.relatedTarget).data('userid');

                    let url = '{{ route("voucher-limited-delete", ":id") }}';
                    url = url.replace(':id', user_id);
                    $('#confirm_delete_form').attr('action', url);

                }else if(type == 'limited_type'){

                    let type_id = $(e.relatedTarget).data('typeid');

                    let url = '{{ route("voucher-limited-type-delete", ":id") }}';
                    url = url.replace(':id', type_id);
                    $('#confirm_delete_form').attr('action', url);
                }else return false


            });


        });



</script>


@endsection


