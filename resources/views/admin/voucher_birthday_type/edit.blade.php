@extends('layouts.app')

@section('admin-logged')

<div class="card-header" style="display:flex; justify-content:space-between">
    <div style="align-self: center;">New Birthday Voucher Type</div>
</div>

<div class="card-body">

    <form id="vbirthday_form" method="POST" action="{{ isset($voucher_type)?route('voucher-birthday-type-update',$voucher_type->id):route('voucher-birthday-type-store') }}" enctype="multipart/form-data">
        @csrf
        @if(isset($voucher_type))
            @method('PUT')
        @endif

        <div class="form-group row">
            <label for="type_name" class="col-md-3 col-form-label text-md-right">Name</label>

            <div class="col-md-6">
                <input id="type_name" type="text" class="form-control @error('type_name') is-invalid @enderror" name="type_name" value="{{isset($voucher_type)?$voucher_type->type_name:'' }}" required autofocus>

                @error('type_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="description" class="col-md-3 col-form-label text-md-right">Description</label>

            <div class="col-md-6">
                <input type="hidden" id="description" name="description" value="{{ isset($voucher_type)? $voucher_type->description:'' }}">
                <trix-editor input="description"></trix-editor>

                @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="image" class="col-md-3 col-form-label text-md-right">Background Image</label>
            <div class="col-md-6">
                <input type="file" id="image" name="image" class="form-control">

                @if(isset($voucher_type))
                <img src="{{isset($voucher_type)? $voucher_type->image:''}}" id="preview-img" width="200px" style="" />
                @endif
            </div>

            @error('image')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

        </div>

        <div class="form-group row">
            <label for="show" class="col-md-3 col-form-label text-md-right">Show at frontend</label>
            <div class="col-md-6">
                <!-- <input type="checkbox" id="show" name="show" value="show" data-onstyle="success" data-offstyle="info" data-toggle="toggle" data-size="small"
                    @if(isset($voucher_type))
                        {{ ( $voucher_type->show) ? 'checked' : '' }}
                    @endif
                > -->
                <select id="show" name="show" class="custom-select custom-select-sm">
                        <option value="2"
                            @if(isset($voucher_type))
                                {{($voucher_type->show == '2'? 'selected':'')}}
                            @endif
                        >My vouher , History</option>
                        <option value="3"
                            @if(isset($voucher_type))
                                {{($voucher_type->show == '3'? 'selected':'')}}
                            @endif
                        >History</option>
                        <option value="0"
                            @if(isset($voucher_type))
                                {{($voucher_type->show == '0'? 'selected':'')}}
                            @endif
                        >Hide all</option>

                </select>
            </div>

        </div>

        <div class="form-group row mb-0">
            <div style="text-align:center; padding-left:50px; padding-right:50px">
                <button type="submit" id="btn_create" class="btn btn-primary">
                <span id="btn_create_text" class="" role="status" aria-hidden="true"></span>
                    {{isset($voucher_type)? 'Update':'Create'}}
                </button>

            </div>
        </div>
    </form>



</div>
@endsection

@section('head')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.min.js" crossorigin="anonymous" referrerpolicy="no-referrer" defer></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>


<script>

    $(document).ready(function() {

        //preview img
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview-img').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#image").change(function(){
            readURL(this);
        });
        //preview img (having img)
        @if(isset($post))
            $('#preview-img').css('display','block');
            $('#preview-img').attr('src', "{{ $post->image }}");
        @endif

        $("#btn_create").click(()=>{
            $("#vbirthday_form").submit();

            $("#btn_create_text").addClass('spinner-border spinner-border-sm');
            $('#btn_create').attr('disabled','disabled');

            $("#vbirthday_form :input").prop('readonly', true);

        })

    });
</script>
@endsection


