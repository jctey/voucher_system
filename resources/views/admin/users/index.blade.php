@extends('layouts.app')

@section('admin-logged')

<div class="card-header" style="display:flex; justify-content:space-between">
    <div style="align-self: center;">User list</div>
    <div>
        <a
            class="btn btn-outline-primary"
            style="box-shadow: 2px 3px 6px 4px #dddddd;"
            href="{{ route('user-create') }}"
        >
            Create Newuser
        </a>
        <button
            type="button"
            class="btn btn-outline-primary"
            data-toggle="modal"
            data-target="#userFilterModalCenter"
        >
            <i class="fa fa-filter" aria-hidden="true"></i>
        </button>
    </div>
</div>

<div class="card-body" style="overflow-x:auto">

    <div class="panel-body">
        <table class="table table-bordered" id="users-table" style="width:100%">
            <thead>
                <tr>
                    <th></th>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Verify</th>
                    <th>DOB</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>

    <!-- modal -->
    @include('partials.modal.user_modal')
    @include('partials.modal.confirm_modal')

</div>
@endsection

@section('head')
<!-- <link href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" rel="stylesheet"> -->
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- <link href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css" rel="stylesheet"> -->
<link rel="https://cdn.datatables.net/rowgroup/1.1.1/css/rowGroup.bootstrap4.min.css" />
@endsection

@section('scripts')
<!-- <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script> -->
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<!-- <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.1.2/handlebars.min.js"></script>


<script id="details-template" type="text/x-handlebars-template">
    @verbatim
        <table class="table">
            <tr>
                <td>Full name:</td>
                <td>{{name}}</td>
            </tr>
            <tr>
                <td>Phone:</td>
                <td>{{phone_number}}</td>
            </tr>
            <!-- <tr>
                <td>Birthday Voucher Code:</td>
                <td>
                    {{voucher_code_newuser}}<span class="ml-2 badge badge-info">{{used}}</span>
                </td>
            </tr> -->
            <tr>
                <td>Created date</td>
                <td>{{created_at}}</td>
            </tr>
        </table>
    @endverbatim
</script>

<script>
        var template = Handlebars.compile($("#details-template").html());
        var table = $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        bLengthChange: true,
        ajax: {
            url:'{{ route('user-list-datatable') }}',
            data: function (d) {
                d.role = $('input[name="role"]:checked').val();
                d.verify = $('input[name="verify"]:checked').val();
            }
        },

        columns: [
            {
                "className":'details-control',
                "orderable":false,
                "searchable":false,
                "data":null,
                "defaultContent":''
            },
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'email_verified_at', name: 'email_verified_at' },
            { data: 'date_of_birth', name: 'date_of_birth' },
            {
                data: 'action',
                name: 'action',
                orderable: true,
                searchable: true
            },
        ],
        order: [[1, 'asc']]
        });

        $(document).ready(function () {

            $('#users-table tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row( tr );
                if ( row.child.isShown() ) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('details');
                }
                else {
                    // Open this row
                    row.child( template(row.data()) ).show();
                    tr.addClass('details');
            }
            });

            $("#filter_btn").click(function(e) {

                table.ajax.reload();
            });

            $("#filter_reset_btn").click(function(e) {

                $('input[name="verify"]:checked').prop('checked', false);
                $('input[name="role"]:checked').prop('checked', false);

                table.ajax.reload();
            });

            $('#confirmModal').on('shown.bs.modal', function(e) {

                //get data-id attribute of the clicked element

                let type = $(e.relatedTarget).data('modaltype');

                if(type == 'user'){

                    let user_id = $(e.relatedTarget).data('userid');
                    let user_email = $(e.relatedTarget).data('useremail');

                    let url = '{{ route("user-delete", ":id") }}';
                    url = url.replace(':id', user_id);
                    $('#confirm_delete_form').attr('action', url);
                }else return false


            });
        });



</script>


@endsection


