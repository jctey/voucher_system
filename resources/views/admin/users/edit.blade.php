@extends('layouts.app')

@section('admin-logged')

<div class="card-header" style="display:flex; justify-content:space-between">
    <div style="align-self: center;">User Detail</div>
</div>

<div class="card-body">

    <form method="POST" action="{{ route('user-update',$user->id) }}">
        @csrf

        <div class="form-group row">
            <label for="name" class="col-md-3 col-form-label text-md-right">Name</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>

                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-md-3 col-form-label text-md-right">Email</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="phone_number" class="col-md-3 col-form-label text-md-right">Phone</label>

            <div class="col-md-6">
                <input name="phone_number" type="text" class="form-control inptFielsd @error('phone_number') is-invalid @enderror" id="phone_number"
                placeholder="Phone Number" value="{{ $user->phone_number }}" maxlength="12"/>

                <input type="hidden" id="phone_full" name="phone_full" maxlength="12"/>
                @error('phone_number')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        @if(isset($user))
            @if($user->is_admin == true)
            <input type="hidden" id="is_admin" name="is_admin" value="is_admin">
            @endif
        @endif

        <div class="form-group row">
            <label for="date_of_birth" class="col-md-3 col-form-label text-md-right">Birthday</label>

            <div class="col-md-6">
                <input id="date_of_birth" type="date" class="form-control @error('date_of_birth') is-invalid @enderror" name="date_of_birth" value="{{ $user->date_of_birth }}">

                @error('date_of_birth')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-md-3 col-form-label text-md-right">{{ __('Password') }}</label>

            <div class="col-md-5">
                <input id="password" placeholder="Remain password leave it empty" type="password" class="form-control @error('password') is-invalid @enderror" name="password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-1">
                <span class="input-group-btn" id="eyeSlash">
                    <button class="btn-password btn btn-outline-dark btn-sm reveal" type="button"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>
                </span>
                <span class="input-group-btn" id="eyeShow" style="display: none;">
                    <button class="btn-password btn btn-outline-dark btn-sm reveal" type="button"><i class="fa fa-eye" aria-hidden="true"></i></button>
                </span>
            </div>
        </div>


        <div class="form-group row">
            <label for="created_at" class="col-md-3 col-form-label text-md-right">Created at</label>

            <div class="col-md-6">
                <input id="created_at" type="text" class="form-control" name="created_at" value="{{ $user->created_at }}" readonly>
            </div>
        </div>

        <div class="form-group row mb-0">
            <div style="text-align:center">
                <button type="submit" class="btn btn-primary">
                    Update
                </button>
            </div>
        </div>
    </form>



</div>
@endsection

@section('head')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endsection

@section('scripts')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- phone dropdown prefix plugin -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"
    integrity="sha512-DNeDhsl+FWnx5B1EQzsayHMyP6Xl/Mg+vcnFPXGNjUZrW28hQaa1+A4qL9M+AiOMmkAhKAWYHh1a+t6qxthzUw=="
    crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.min.css"
    integrity="sha512-yye/u0ehQsrVrfSd6biT17t39Rg9kNc+vENcCXZuMz2a+LWFGvXUnYuWUW6pbfYj1jcBb/C39UZw2ciQvwDDvg=="
    crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js"
    integrity="sha512-BNZ1x39RMH+UYylOW419beaGO0wqdSkO7pi1rYDYco9OL3uvXaC/GTqA5O4CVK2j4K9ZkoDNSSHVkEQKkgwdiw=="
    crossorigin="anonymous"></script>

    <script>

        var input = document.querySelector("#phone_number");

        var phone_num =window.intlTelInput(input, {
            onlyCountries: ["my", "sg"],
            separateDialCode: true,
            utilsScript : "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/utils.min.js",
            customPlaceholder: function (
                selectedCountryPlaceholder,
                selectedCountryData
            ) {
                return "e.g. " + selectedCountryPlaceholder;
            },
        });

        $("form").submit(function() {
            var full_number = phone_num.getNumber(intlTelInputUtils.numberFormat.E164);
            $("#phone_number").val(full_number);
        });

        $("#phone_number").keydown(function (e) {

            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||

                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||

                (e.keyCode >= 35 && e.keyCode <= 40)) {

                return;
            }

            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $('.btn-password').click(()=>{
            var x = document.getElementById('password');
            if (x.type === 'password') {
                x.type = "text";
                $('#eyeShow').show();
                $('#eyeSlash').hide();
            }else {
                x.type = "password";
                $('#eyeShow').hide();
                $('#eyeSlash').show();
            }
        });


    </script>
@endsection

