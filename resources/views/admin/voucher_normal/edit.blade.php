@extends('layouts.app')

@section('admin-logged')

<div class="card-header" style="display:flex; justify-content:space-between">
    <div style="align-self: center;">Detail</div>
</div>

<div class="card-body">

    <form method="POST" action="{{ route('voucher-normal-update',$normal->id) }}">
        @csrf
        @if(isset($normal))
            @method('PUT')
        @endif
        <div class="form-group row">
            <label for="voucher_name" class="col-md-3 col-form-label text-md-right">Voucher Name</label>

            <div class="col-md-6">
                <input id="voucher_name" type="text" class="form-control @error('voucher_name') is-invalid @enderror" name="voucher_name" value="{{ $normal->vouchernormalType->type_name }}"  readonly>

                @error('voucher_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="voucher_code" class="col-md-3 col-form-label text-md-right">Voucher Code</label>

            <div class="col-md-6">
                <input id="voucher_code" type="text" class="form-control @error('voucher_code') is-invalid @enderror" name="voucher_code" value="{{ $normal->voucher_code }}"  readonly>

                @error('voucher_code')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="visible" class="col-md-3 col-form-label text-md-right">Visible for user</label>
            <div class="col-md-6">
                <input type="checkbox" id="visible" name="visible" value="visible" data-onstyle="success" data-offstyle="info" data-toggle="toggle" data-size="small"
                    @if(isset($normal))
                        {{ ( $normal->visible) ? 'checked' : '' }}
                    @endif
                >
            </div>

        </div>


        <div class="form-group row mb-0">
            <div style="text-align:center">
                <button type="submit" class="btn btn-primary">
                    Update
                </button>
            </div>
        </div>
    </form>



</div>
@endsection

@section('head')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endsection

@section('scripts')

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

@endsection
