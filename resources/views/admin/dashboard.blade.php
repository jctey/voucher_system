@extends('layouts.app')

@section('admin-logged')

<div class="card-header">Admin Dashboard</div>

<div class="card-body">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}

        </div>
    @endif


    <!-- chart user -->
    <div class="chartWrapper">
      <div class="chartAreaWrapperOuter1">
        <div class="chartAreaWrapperInner1">
            <canvas id="userChart"></canvas>
        </div>
      </div>

        <!-- <canvas id="userChartAxis" height="300" width="0"></canvas> -->

        <div class="dashboard_total_text"><span>Total Number of User: <strong>{{$total_user}}</strong></span></div>
    </div>
    <!--  -->

    <!-- chart register voucher -->
    <div class="chartWrapper mt-4">
      <div class="chartAreaWrapperOuter2">
        <div class="chartAreaWrapperInner2">
            <canvas id="registerChart"></canvas>
        </div>
      </div>

        <!-- <canvas id="registerChartAxis" height="300" width="0"></canvas> -->

        <div class="dashboard_total_text"><span>Total Number Used: <strong>{{$total_register_v}}</strong></span></div>
    </div>
    <!--  -->

    <!-- chart birthday voucher -->
    <div class="chartWrapper mt-4">
      <div class="chartAreaWrapperOuter3">
        <div class="chartAreaWrapperInner3">
            <canvas id="birthdayChart"></canvas>
        </div>
      </div>

        <!-- <canvas id="birthdayChartAxis" height="300" width="0"></canvas> -->

        <div class="dashboard_total_text"><span>Total Number Used: <strong>{{$total_birthday_v}}</strong></span></div>
    </div>
    <!--  -->

    <!-- chart limited voucher -->
    <div class="chartWrapper mt-4">
      <div class="chartAreaWrapperOuter4">
        <div class="chartAreaWrapperInner4">
            <canvas id="limitedChart"></canvas>
        </div>
      </div>

        <!-- <canvas id="limitedChartAxis" height="300" width="0"></canvas> -->

        <div class="dashboard_total_text"><span>Total Number Used: <strong>{{$total_limited_v}}</strong></span></div>
    </div>
    <!--  -->

    <!-- chart normal voucher -->
    <div class="chartWrapper mt-4">
      <div class="chartAreaWrapperOuter5">
        <div class="chartAreaWrapperInner5">
            <canvas id="normalChart"></canvas>
        </div>
      </div>

        <!-- <canvas id="normalChartAxis" height="300" width="0"></canvas> -->

        <div class="dashboard_total_text"><span>Total Number Used: <strong>{{$total_normal_v}}</strong></span></div>
    </div>
    <!--  -->

</div>
@endsection

@section('scripts')
<!-- Charting library -->
<script src="https://unpkg.com/chart.js@^2.9.3/dist/Chart.min.js"></script>
<!-- Chartisan -->
<script src="https://unpkg.com/@chartisan/chartjs@^2.1.0/dist/chartisan_chartjs.umd.js"></script>
<script>



    // var chartdata = {
    //     type: 'bar',
    //     data: {
    //         labels: <?php echo json_encode($period_day); ?>,
    //         // labels: month,
    //         datasets: [
    //             {
    //                 label: 'this year',
    //                 backgroundColor: '#26B99A',
    //                 borderWidth: 1,
    //                 data: <?php echo json_encode($user_data); ?>
    //             }
    //         ]
    //     },
    //     options: {
    //         scales: {
    //             yAxes: [{
    //                 ticks: {
    //                     beginAtZero:true
    //                 }
    //             }]
    //         }
    //     }
    // }
    // var ctx = document.getElementById('canvas').getContext('2d');
    // new Chart(ctx, chartdata);

    //

    //user chart
    var ctx = document.getElementById("userChart").getContext("2d");
    var chart = {
        options: {
          responsive: true,
          maintainAspectRatio: false,
          scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true,
                        stepSize: 1,
                    }
                }]
          },
          animation: {
            onComplete: function(animation) {
                var sourceCanvas = myLiveChart.chart.canvas;
                var copyWidth = myLiveChart.scales['y-axis-0'].width - 10;
                var copyHeight = myLiveChart.scales['y-axis-0'].height + myLiveChart.scales['y-axis-0'].top + 10;
                // var targetCtx = document.getElementById("userChartAxis").getContext("2d");
                // targetCtx.canvas.width = copyWidth;
                // targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth, copyHeight, 0, 0, copyWidth, copyHeight);
            }
          }
        },
        type: 'bar',
        data: {
            labels: <?php echo json_encode($period_day); ?>,
            datasets: [
                {
                    label: "New User Register (30 Days)",
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    backgroundColor: '#26B99A',
                    data: <?php echo json_encode($user_data); ?>
                }
            ]
        }
    };
    var myLiveChart = new Chart(ctx, chart);
    // myLiveChart.data.datasets[0].data.push(Math.random() * 100);
    // myLiveChart.data.labels.push("Date");
    var newwidth = $('.chartAreaWrapperInner1').width() +60;
    $('.chartAreaWrapperInner1').width(newwidth);
    $('.chartAreaWrapperOuter1').animate({scrollLeft:newwidth});

    //register chart
    var ctx2 = document.getElementById("registerChart").getContext("2d");
    var chart2 = {
        options: {
          responsive: true,
          maintainAspectRatio: false,
          scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true,
                        stepSize: 1,
                    }
                }]
          },
          animation: {
            onComplete: function(animation) {
                var sourceCanvas = myLiveChart2.chart.canvas;
                var copyWidth = myLiveChart2.scales['y-axis-0'].width - 10;
                var copyHeight = myLiveChart2.scales['y-axis-0'].height + myLiveChart2.scales['y-axis-0'].top + 10;
                // var targetCtx = document.getElementById("registerChartAxis").getContext("2d");
                // targetCtx.canvas.width = copyWidth;
                // targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth, copyHeight, 0, 0, copyWidth, copyHeight);
            }
          }
        },
        type: 'bar',
        data: {
            labels: <?php echo json_encode($period_day); ?>,
            datasets: [
                {
                    label: "Used of Vouher Registered (30 Days)",
                    fillColor: "rgba(51,51,255,0.6)",
                    strokeColor: "rgba(51,51,255,1)",
                    pointColor: "rgba(51,51,255,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(51,51,255,1)",
                    backgroundColor: '#005afb',
                    data: <?php echo json_encode($register_data); ?>
                }
            ]
        }
    };
    var myLiveChart2 = new Chart(ctx2, chart2);
    var newwidth2 = $('.chartAreaWrapperInner2').width() +60;
    $('.chartAreaWrapperInner2').width(newwidth2);
    $('.chartAreaWrapperOuter2').animate({scrollLeft:newwidth2});

    // birthday chart
    var ctx3 = document.getElementById("birthdayChart").getContext("2d");
    var chart3 = {
        options: {
          responsive: true,
          maintainAspectRatio: false,
          scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true,
                        stepSize: 1,
                    }
                }]
          },
          animation: {
            onComplete: function(animation) {
                var sourceCanvas = myLiveChart3.chart.canvas;
                var copyWidth = myLiveChart3.scales['y-axis-0'].width - 10;
                var copyHeight = myLiveChart3.scales['y-axis-0'].height + myLiveChart3.scales['y-axis-0'].top + 10;
                // var targetCtx = document.getElementById("birthdayChartAxis").getContext("2d");
                // targetCtx.canvas.width = copyWidth;
                // targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth, copyHeight, 0, 0, copyWidth, copyHeight);
            }
          }
        },
        type: 'bar',
        data: {
            labels: <?php echo json_encode($period_day); ?>,
            datasets: [
                {
                    label: "Used of Vouher Birthday (30 Days)",
                    fillColor: "rgba(51,51,255,0.6)",
                    strokeColor: "rgba(51,51,255,1)",
                    pointColor: "rgba(51,51,255,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(51,51,255,1)",
                    backgroundColor: '#a376fb',
                    data: <?php echo json_encode($birthday_data); ?>
                }
            ]
        }
    };
    var myLiveChart3 = new Chart(ctx3, chart3);
    var newwidth3 = $('.chartAreaWrapperInner3').width() +60;
    $('.chartAreaWrapperInner3').width(newwidth3);
    $('.chartAreaWrapperOuter3').animate({scrollLeft:newwidth3});

    // limited chart
    var ctx4 = document.getElementById("limitedChart").getContext("2d");
    var chart4 = {
        options: {
          responsive: true,
          maintainAspectRatio: false,
          scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true,
                        stepSize: 1,
                    }
                }]
          },
          animation: {
            onComplete: function(animation) {
                var sourceCanvas = myLiveChart4.chart.canvas;
                var copyWidth = myLiveChart4.scales['y-axis-0'].width - 10;
                var copyHeight = myLiveChart4.scales['y-axis-0'].height + myLiveChart4.scales['y-axis-0'].top + 10;
                // var targetCtx = document.getElementById("limitedChartAxis").getContext("2d");
                // targetCtx.canvas.width = copyWidth;
                // targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth, copyHeight, 0, 0, copyWidth, copyHeight);
            }
          }
        },
        type: 'bar',
        data: {
            labels: <?php echo json_encode($period_day); ?>,
            datasets: [
                {
                    label: "Used of Vouher Limited (30 Days)",
                    fillColor: "rgba(51,51,255,0.6)",
                    strokeColor: "rgba(51,51,255,1)",
                    pointColor: "rgba(51,51,255,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(51,51,255,1)",
                    backgroundColor: '#fac433',
                    data: <?php echo json_encode($limited_data); ?>
                }
            ]
        }
    };
    var myLiveChart4 = new Chart(ctx4, chart4);
    var newwidth4 = $('.chartAreaWrapperInner4').width() +60;
    $('.chartAreaWrapperInner4').width(newwidth4);
    $('.chartAreaWrapperOuter4').animate({scrollLeft:newwidth4});

    //normal chart
    var ctx5 = document.getElementById("normalChart").getContext("2d");
    var chart5 = {
        options: {
          responsive: true,
          maintainAspectRatio: false,
          scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true,
                        stepSize: 1,
                    }
                }]
          },
          animation: {
            onComplete: function(animation) {
                var sourceCanvas = myLiveChart5.chart.canvas;
                var copyWidth = myLiveChart5.scales['y-axis-0'].width - 10;
                var copyHeight = myLiveChart5.scales['y-axis-0'].height + myLiveChart5.scales['y-axis-0'].top + 10;
                // var targetCtx = document.getElementById("normalChartAxis").getContext("2d");
                // targetCtx.canvas.width = copyWidth;
                // targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth, copyHeight, 0, 0, copyWidth, copyHeight);
            }
          }
        },
        type: 'bar',
        data: {
            labels: <?php echo json_encode($period_day); ?>,
            datasets: [
                {
                    label: "Used of Vouher Normal (30 Days)",
                    fillColor: "rgba(51,51,255,0.6)",
                    strokeColor: "rgba(51,51,255,1)",
                    pointColor: "rgba(51,51,255,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(51,51,255,1)",
                    backgroundColor: '#24ff33',
                    data: <?php echo json_encode($normal_data); ?>
                }
            ]
        }
    };
    var myLiveChart5 = new Chart(ctx5, chart5);
    var newwidth5 = $('.chartAreaWrapperInner5').width() +60;
    $('.chartAreaWrapperInner5').width(newwidth5);
    $('.chartAreaWrapperOuter5').animate({scrollLeft:newwidth5});


</script>
@endsection

