<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\ISMS;

class sendSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries =3;
    public $backoff =5;
    public $maxExceptions =2;

    protected $phone,$msg;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($phone,$msg)
    {
        $this->phone = $phone;
        $this->msg = $msg;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ISMS::_SendSms($this->phone, $this->msg, "2");
    }


}
