<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Session;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use App\Models\VoucherLimitedType;
use App\Models\VoucherLimited;
use App\Models\VoucherNormalType;
use App\Models\VoucherNormal;


class GetVoucher implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries =3;
    public $backoff =5;
    public $maxExceptions =2;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $voucher_type,$voucher_type_id,$user_id;

    public function __construct($voucher_type,$voucher_type_id,$user_id)
    {
        $this->voucher_type = $voucher_type;
        $this->voucher_type_id = $voucher_type_id;
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if($this->voucher_type){
            // voucher_type :   limited , normal

            if($this->voucher_type == 'normal'){

                if($this->voucher_type_id){

                    //check voucher type
                    if (VoucherNormalType::where('id', '=', $this->voucher_type_id)->exists()) {

                        if (VoucherNormal::where('voucher_type_id',$this->voucher_type_id)->where('user_id', '=', $this->user_id)->exists()) {
                            Session::flash('error','You get this voucher before');
                            return false;
                        }else{
                            $this->check_update_voucher_normal($this->voucher_type_id);
                            return redirect()->back();
                        }

                    }else {
                        Session::flash('error','Voucher doesnt exist');
                        redirect()->back();
                        return false;
                    }

                }else {
                    Session::flash('error','No voucher id provided');
                    redirect()->back();
                    return false;
                }

            }else if($this->voucher_type == 'limited'){

                if($this->voucher_type_id){

                    $voucherType = VoucherLimitedType::where('id', '=', $this->voucher_type_id);
                    //check voucher type
                    if (VoucherLimitedType::where('id', '=', $this->voucher_type_id)->exists()) {

                        if (VoucherLimited::where('voucher_type_id',$this->voucher_type_id)->where('user_id', '=', $this->user_id)->exists()) {
                            Session::flash('error','You get this voucher before');
                            return false;
                        }else{
                            //check expired
                            $expiredCheck = VoucherLimitedType::where('id', '=', $this->voucher_type_id)
                                        ->where('publish_date', '<=', date('Y-m-d H:i:s'))
                                        ->where('unpublish_date', '>=', date('Y-m-d H:i:s'))
                                        ->exists();
                            if($expiredCheck){
                                $this->check_update_voucher_limited($this->voucher_type_id);
                                return redirect()->back();
                            }else{
                                Session::flash('error','You expired');
                                return false;
                            }


                        }

                    }else{
                        Session::flash('error','Voucher doesnt exist');
                        redirect()->back();
                        return false;
                    }

                }else {
                    Session::flash('error','No Voucher is fully redeemid provided');
                    redirect()->back();
                }

            }else {
                Session::flash('error','No voucher type provided');
                redirect()->back();
            }

        }

    }

    public function check_update_voucher_limited()
    {
        $voucher_limited = VoucherLimited::where('voucher_type_id',$this->voucher_type_id)->where('user_id',null)->first();

        if (is_null($voucher_limited)) {
            Session::flash('error','Voucher is fully redeem');
            return false;
        }else{
            //update voucher
            $voucher_limited->user_id = $this->user_id;

            $voucher_limited->save();

            Session::flash('success','Successfully get a voucher');
            redirect()->back();
        }

        //dd($client_voucher_code = $voucher_limited->voucher_code);
    }

    public function check_update_voucher_normal()
    {
        $voucher_normal = VoucherNormal::where('voucher_type_id',$this->voucher_type_id)->where('user_id',null)->first();

        if (is_null($voucher_normal)) {
            Session::flash('error','Voucher is fully redeem');
            return false;
        }else{
            //update voucher

            $voucher_normal->user_id = $this->user_id;

            $voucher_normal->save();

            Session::flash('success','Successfully get a voucher');
            redirect()->back();
        }

        //dd($client_voucher_code = $voucher_limited->voucher_code);
    }

    public function middleware()
    {
        return[
            (new WithoutOverLapping($this->user_id))->dontRelease()
        ];
    }
}
