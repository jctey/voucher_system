<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ISMS extends Model
{
    private static $username;
    private static $password;
    private static $url;

    public function __construct() {
        self::$username = urlencode(config('services.isms.username'));
        self::$password = urlencode(config('services.isms.password'));
        self::$url = 'https://www.isms.com.my/';
    }

    protected static function _SendSms($destination, $message, $type) {

        $destination = static::formatPhoneNum($destination);

        $message = html_entity_decode($message, ENT_QUOTES, 'utf-8');
        $message = urlencode($message);

        $type = (int) $type;
        $link = self::$url . "isms_send_all.php";
        $link .= "?un=".self::$username."&pwd=".self::$password."&dstno=".$destination."&msg=".$message."&type=".$type;

        $result = static::postcURL($link);
        return $result;
    }

    protected static function CheckBalance() {
        $link = static::$url;
        $link .= "isms_balance.php?un=" . urlencode(static::$username) . "&pwd=" . urlencode(static::$password);
        $result = static::postcURL($link);
        return $result;
    }

    public static function postcURL($link) {

        $http = curl_init($link);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        $response = array();
        $response['http_result'] = $http_result;
        $response['http_status'] = $http_status;

        return ($response);
    }

    public static function formatPhoneNum($phone){
        $phone = preg_replace("/[^0-9]*/",'',$phone);
        return $phone;
    }
}
