<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class VoucherLimitedType extends Model
{
    use HasFactory;

    public $table = "voucher_type_limited";

    protected $fillable = [
        'id',
        'type_name',
        'description',
        'show',
        'image',
        'publish_date',
        'unpublish_date',
        'mark_used',
        'created_at'
    ];

    public function getImageAttribute($image){
        return asset($image);
    }

    public function scopeCheckShow($query,$show){

        return $query->where('show','=', $show);

    }

    public function voucherLimited()
    {
        return $this->hasMany(VoucherLimited::class);
    }

    public function scopePublished($query){

        return $query->where('publish_date','<=',now())
                    ->where('unpublish_date','>=',now());
    }

}
