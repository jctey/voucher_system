<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherNewuser extends Model
{
    use HasFactory;

    public $table = "voucher_newuser";

    protected $fillable = [
        'user_id',
        'voucher_code',
        'status',
        'used',
        'voucher_type_id',
        'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function voucherNewuserType()
    {
        return $this->belongsTo(VoucherNewuserType::class,'voucher_type_id');
    }

}
