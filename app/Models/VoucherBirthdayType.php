<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherBirthdayType extends Model
{
    use HasFactory;

    public $table = "voucher_type_birthday";

    protected $fillable = [
        'id',
        'type_name',
        'description',
        'show',
        'image',
        'created_at'
    ];

    public function voucherBirthday()
    {
        return $this->hasMany(VoucherBirthday::class);
    }

    public function getImageAttribute($image){
        return asset($image);
    }
}
