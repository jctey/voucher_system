<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'id',
        'name',
        'email',
        'phone_number',
        'date_of_birth',
        'is_admin',
        'password',
        'email_verified_at',
        'created_at',
        'updated_at'
    ];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin()
    {
        return $this->is_admin;
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new \App\Notifications\VerifyEmailQueued);
    }

    public function voucherNewuser()
    {
        return $this->hasMany(VoucherNewuser::class);
    }

    public function voucherBirthday()
    {
        return $this->hasMany(VoucherBirthday::class);
    }

    public function voucherLimited()
    {
        return $this->hasMany(VoucherLimited::class);
    }

    public function voucherNormal()
    {
        return $this->hasMany(VoucherNormal::class);
    }

    public function generateVoucherCode()
    {
        $length = 8;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function scopeGetVoucher($query,$user)
    {
        $v_registed_type = VoucherNewuserType::get();
        $v_birthday_type = VoucherBirthdayType::get();

        if(count($v_registed_type) > 0){
            foreach ($v_registed_type as $type) {
                $user->voucherNewuser()->create([
                    'voucher_code' => $user->generateVoucherCode(),
                    'status' => 'inactive',
                    'used' => false,
                    'user_id' => $user->id,
                    'voucher_type_id' => $type->id
                ]);
            }
        }

        if(count($v_birthday_type) > 0){
            foreach ($v_birthday_type as $type) {
                $user->voucherBirthday()->create([
                    'voucher_code' => $user->generateVoucherCode(),
                    'status' => 'inactive',
                    'used' => false,
                    'user_id' => $user->id,
                    'voucher_type_id' => $type->id
                ]);
            }
        }


    }

    public function scopeCheckHaveLimitedVoucher($query,$type_id)
    {
        $vlimited = VoucherLimited::where('user_id','=',auth()->user()->id)
            ->where('voucher_type_id','=',$type_id)
            ->exists();

        if($vlimited) return true;
        else return false;
    }

    public function scopeCheckHaveLimitedVoucherAvailable($query,$type_id)
    {
        $vlimited = VoucherLimited::where('voucher_type_id','=',$type_id)
            ->where('user_id','=',null)
            ->exists();

        if($vlimited) return true;
        else return false;
    }

    public function scopeCheckHaveNormalVoucher($query,$type_id)
    {
        $vnormal = VoucherNormal::where('user_id','=',auth()->user()->id)
            ->where('voucher_type_id','=',$type_id)
            ->exists();

        if($vnormal) return true;
        else return false;
    }

    public function scopeCheckHaveNormalVoucherAvailable($query,$type_id)
    {
        $vnormal = VoucherNormal::where('voucher_type_id','=',$type_id)
            ->where('user_id','=',null)
            ->exists();

        if($vnormal) return true;
        else return false;
    }

    public function ShowBeforeOneWeek()
    {
        $user_birthday = auth()->user()->date_of_birth;

        $birthday_this_year = strtotime(date('Y')   . '-' . date('m-d', strtotime($user_birthday))); //19-10-2021

        $oneWeekBefore = strtotime(date("Y-m-d", $birthday_this_year) . " -1 week"); //12-10-2021

        $currentDate = strtotime(date('Y-m-d')); //24-10-2021

        if(($oneWeekBefore <= $currentDate) && ($birthday_this_year >= $currentDate) ){
            return true;
        }else return false;

    }
}
