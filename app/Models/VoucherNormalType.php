<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherNormalType extends Model
{
    use HasFactory;

    public $table = "voucher_type_normal";

    protected $fillable = [
        'id',
        'type_name',
        'description',
        'show',
        'image',
        'mark_used',
        'created_at'
    ];

    public function getImageAttribute($image){
        return asset($image);
    }

    public function voucherNormal()
    {
        return $this->hasMany(VoucherNormal::class);
    }
}
