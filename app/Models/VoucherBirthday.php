<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherBirthday extends Model
{
    use HasFactory;

    public $table = "voucher_birthday";

    protected $fillable = [
        'user_id',
        'voucher_code',
        'status',
        'used',
        'voucher_type_id',
        'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function voucherBirthdayType()
    {
        return $this->belongsTo(VoucherBirthdayType::class,'voucher_type_id');
    }
}
