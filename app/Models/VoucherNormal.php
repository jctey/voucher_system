<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherNormal extends Model
{
    use HasFactory;

    public $table = "voucher_normal";

    protected $fillable = [
        'id',
        'voucher_code',
        'user_id',
        'voucher_type_id',
        'status',
        'visible',
        'used',
        'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function voucherNormalType()
    {
        return $this->belongsTo(VoucherNormalType::class,'voucher_type_id');
    }

    public function existVoucherOf($type_id)
    {
        $voucherExist = VoucherNormal::where('user_id','=',auth()->user()->id)
            ->where('voucher_type_id','=',$type_id)
            ->exists();
        if($voucherExist) return true;
        else return false;
    }

    // public function scopeGetMyVoucher($query){

    //     return $query->where('user_id','=',auth()->user()->id)
    //                     ->where('used','=','0');

    // }
}
