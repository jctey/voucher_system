<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherNewuserType extends Model
{
    use HasFactory;

    public $table = "voucher_type_newuser";

    protected $fillable = [
        'id',
        'type_name',
        'description',
        'show',
        'image',
        'created_at'
    ];

    public function voucherNewuser()
    {
        return $this->hasMany(VoucherNewuser::class);
    }

    public function getImageAttribute($image){
        return asset($image);
    }
}
