<?php

namespace App\Http\Requests\user;

use Illuminate\Foundation\Http\FormRequest;

class updateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        if ($this->request->get('is_admin') == 'is_admin') {
            return [
                'name' => ['required', 'string', 'max:255'],
                'email' => 'required|string|email|max:255|unique:users,email,'.$this->id,

            ];
        }else{
            return [
                'name' => ['required', 'string', 'max:255'],
                'email' => 'required|string|email|max:255|unique:users,email,'.$this->id,
                'phone_number'=> ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/','min:10'],
                'date_of_birth'=>['required','date'],

            ];
        }

    }
}
