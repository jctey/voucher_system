<?php

namespace App\Http\Requests\newuserType;

use Illuminate\Foundation\Http\FormRequest;

class createNewuserTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'type_name'=>'required|unique:voucher_type_newuser',
            'type_name'=>'required',
            'image'=>'image|mimes:jpg,png,jpeg|max:2048|dimensions:min_width=100,min_height=100,max_width=2000,max_height=2000',
        ];
    }

    public function messages()
    {
        return [
            'image.image' => 'File uploaded should be an Image',
            'image.mime' => 'Image uploaded only allow (jpg,png,jpeg)'
        ];
    }
}
