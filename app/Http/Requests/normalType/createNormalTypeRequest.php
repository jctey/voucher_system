<?php

namespace App\Http\Requests\normalType;

use Illuminate\Foundation\Http\FormRequest;

class createNormalTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type_name'=>'required',
            'voucher_number'=>'numeric|required',
            'image'=>'image|mimes:jpg,png,jpeg|max:2048|dimensions:min_width=100,min_height=100,max_width=2000,max_height=2000',
        ];
    }

    public function messages()
    {
        return [
            'voucher_number.numeric' => 'Voucher number only allow numeric',
            'voucher_number.required' => 'Voucher number is required',
            'image.image' => 'File uploaded should be an Image',
            'image.mime' => 'Image uploaded only allow (jpg,png,jpeg)'
        ];
    }
}
