<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ISMS;
use App\Models\User;
use App\Models\VoucherLimitedType;
use App\Models\VoucherLimited;
use App\Http\Requests\limitedType\createLimitedTypeRequest;
use App\Http\Requests\limitedType\updateLimitedTypeRequest;
use Session;
use App\Jobs\sendSMS;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class VoucherLimitedTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('adminCheck');
    }

    public function create()
    {
        return view('admin.voucher_limited_type.edit');
    }

    public function store(createLimitedTypeRequest $request)
    {
        if($request->hasFile('image')){

            $image = $request->image;
            $imageName = time().'.'.$request->image->getClientOriginalName();
            $image->move(public_path('uploads/voucher_limited_type'), $imageName);
        }else{
            $imageName="default.png";
        }

        $show = 0;

        if($request->show) $show = $request->show;
        if($request->description) $description = $request->description;
        else $description ='';

        //create voucher type
        $voucher_limited_type = VoucherLimitedType::create([
            'type_name' => $request->type_name,
            'show' => $show,
            'description' => $description,
            'image' => 'uploads/voucher_limited_type/'.$imageName,
            'publish_date' => $request->publish_date,
            'unpublish_date' => $request->unpublish_date
        ]);

        if($voucher_limited_type){

            //$this->sendSMS();

            //create list of voucher (based on number)
            if($request->voucher_number){

                $user = new User;

                for ($x = 0; $x < $request->voucher_number; $x++)
                {
                    $voucher_limited_list = VoucherLimited::create([
                        'voucher_code' => $user->generateVoucherCode(),
                        'voucher_type_id' => $voucher_limited_type->id,
                    ]);
                }

            }
        }

        Session::flash('success','New type created successfully');

        return redirect(route('voucher-limited-index'));
    }

    public function sendSMS()
    {
        $msg = 'hello';
        // ISMS::CheckBalance();
        $userReceiveSMS = User::where('is_admin',0)
                        ->whereNotNull('phone_number')
                        ->whereNotNull('email_verified_at')
                        ->get();

        if($userReceiveSMS->count() > 0){
            foreach($userReceiveSMS as $user){
                sendSMS::dispatch($user->phone_number,$msg);
                //ISMS::_SendSms($user->phone_number,  $msg, "2");
            }
        }

    }


    public function listDatatable()
    {
        $voucher_types = VoucherLimitedType::all();

        return Datatables::of($voucher_types)
            ->addIndexColumn()
            ->addColumn('action', function ($user) {
                return '<div style="display:flex; justify-content: flex-start;"><a href="'.route('voucher-limited-type-edit',$user->id).'" class="edit btn btn-info btn-sm shadow"> Edit</a><button data-toggle="modal" data-target="#confirmModal" data-modaltype="limited_type" data-typeid='.$user->id.' class="ml-2 delete btn btn-danger btn-sm shadow">Delete</button></div>';
            })

            ->editColumn('id', '{{$id}}')

            ->rawColumns(['action'])
            ->setRowId('id')
            ->setRowClass(function ($user) {
                return 'alert-secondary';
            })

            ->make(true);
    }

    public function edit($id)
    {
        return view('admin.voucher_limited_type.edit')
                ->with('voucher_type',VoucherLimitedType::findOrFail($id));;
    }

    public function update(updateLimitedTypeRequest $request, $id)
    {
        $voucher_limited_type = VoucherLimitedType::findOrFail($id);

        if($request->hasFile('image')){

            $image = $request->image;
            $imageName = time().'.'.$request->image->getClientOriginalName();
            $image->move(public_path('uploads/voucher_limited_type'), $imageName);

            $voucher_limited_type->image = 'uploads/voucher_limited_type/'.$imageName;
        }

        $show = 0;

        if($request->show) $show = $request->show;
        if($request->mark_used){
            $voucher_limited_type->mark_used = true;
            $this->updateMarkUsed($id,'to_used');
        }else {
            $voucher_limited_type->mark_used = false;
            $this->updateMarkUsed($id,'to_unused');
        }

        $voucher_limited_type->type_name = $request->type_name;
        $voucher_limited_type->show = $show;
        $voucher_limited_type->description = $request->description;
        $voucher_limited_type->publish_date = $request->publish_date;
        $voucher_limited_type->unpublish_date = $request->unpublish_date;

        $voucher_limited_type->save();

        Session::flash('success','Type updated successfully');

        return redirect(route('voucher-limited-index'));;
    }

    public function updateMarkUsed($type_id,$status)
    {
        $voucher_limited = VoucherLimited::where('voucher_type_id','=',$type_id)
                                        ->whereNull('user_id');
        if($voucher_limited->count() > 0){

            if($status == 'to_used') $voucher_limited->update(array('used' => true));
            if($status == 'to_unused') $voucher_limited->update(array('used' => false));

        }
    }

    public function delete($id)
    {

        if(isset($id)){

            try{
                $user = VoucherLimitedType::findOrFail($id);
                $user->delete();

                Session::flash('success','Type deleted');

                return redirect()->back();

            }catch(Exception $e) {
                return Session::flash('error','Something wrong');
            }

        }

    }
}
