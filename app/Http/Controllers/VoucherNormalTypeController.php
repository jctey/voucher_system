<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ISMS;
use App\Models\User;
use App\Models\VoucherNormalType;
use App\Models\VoucherNormal;
use App\Http\Requests\normalType\createNormalTypeRequest;
use App\Http\Requests\normalType\updateNormalTypeRequest;
use Session;
use App\Jobs\sendSMS;

use DataTables;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class VoucherNormalTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('adminCheck');
    }

    public function create()
    {
        return view('admin.voucher_normal_type.edit');
    }

    public function store(createNormalTypeRequest $request)
    {

        if($request->hasFile('image')){

            $image = $request->image;
            $imageName = time().'.'.$request->image->getClientOriginalName();
            $image->move(public_path('uploads/voucher_normal_type'), $imageName);
        }else{
            $imageName="default.png";
        }

        $show = 0;

        if($request->show) $show = $request->show;

        //create voucher type
        $voucher_normal_type = VoucherNormalType::create([
            'type_name' => $request->type_name,
            'show' => $show,
            'image' => 'uploads/voucher_normal_type/'.$imageName,
            'publish_date' => $request->publish_date,
            'unpublish_date' => $request->unpublish_date
        ]);

        if($voucher_normal_type){

            $this->sendSMS();

            //create list of voucher (based on number)
            if($request->voucher_number){

                $user = new User;

                for ($x = 0; $x < $request->voucher_number; $x++)
                {
                    $voucher_normal_list = VoucherNormal::create([
                        'voucher_code' => $user->generateVoucherCode(),
                        'voucher_type_id' => $voucher_normal_type->id,
                    ]);
                }

            }
        }

        Session::flash('success','New type created successfully');

        return redirect(route('voucher-normal-index'));
    }

    public function sendSMS()
    {
        $msg = 'hello';
        // ISMS::CheckBalance();
        $userReceiveSMS = User::where('is_admin',0)
                        ->whereNotNull('phone_number')
                        ->whereNotNull('email_verified_at')
                        ->get();

        if($userReceiveSMS->count() > 0){
            foreach($userReceiveSMS as $user){
                sendSMS::dispatch($user->phone_number,$msg);
                //ISMS::_SendSms($user->phone_number,  $msg, "2");
            }
        }

    }

    public function listDatatable()
    {
        $voucher_types = VoucherNormalType::all();

        return Datatables::of($voucher_types)
            ->addIndexColumn()
            ->addColumn('action', function ($user) {
                return '<div style="display:flex; justify-content: flex-start;"><a href="'.route('voucher-normal-type-edit',$user->id).'" class="edit btn btn-info btn-sm shadow"> Edit</a><button data-toggle="modal" data-target="#confirmModal" data-modaltype="normal_type" data-typeid='.$user->id.' class="ml-2 delete btn btn-danger btn-sm shadow">Delete</button></div>';
            })

            ->editColumn('id', '{{$id}}')

            ->rawColumns(['action'])
            ->setRowId('id')
            ->setRowClass(function ($user) {
                return 'alert-secondary';
            })

            ->make(true);
    }

    public function edit($id)
    {
        return view('admin.voucher_normal_type.edit')
                ->with('voucher_type',VoucherNormalType::findOrFail($id));;
    }

    public function update(updateNormalTypeRequest $request, $id)
    {
        $voucher_normal_type = VoucherNormalType::findOrFail($id);

        if($request->hasFile('image')){

            $image = $request->image;
            $imageName = time().'.'.$request->image->getClientOriginalName();
            $image->move(public_path('uploads/voucher_normal_type'), $imageName);

            $voucher_normal_type->image = 'uploads/voucher_normal_type/'.$imageName;
        }

        $show = 0;

        if($request->show) $show = $request->show;

        $voucher_normal_type->type_name = $request->type_name;
        $voucher_normal_type->show = $show;
        $voucher_normal_type->description = $request->description;

        if($request->mark_used){
            $voucher_normal_type->mark_used = true;
            $this->updateMarkUsed($id,'to_used');
        }else {
            $voucher_normal_type->mark_used = false;
            $this->updateMarkUsed($id,'to_unused');
        }

        $voucher_normal_type->save();

        Session::flash('success','Type updated successfully');

        return redirect(route('voucher-normal-index'));;
    }

    public function updateMarkUsed($type_id,$status)
    {
        $voucher_normal = VoucherNormal::where('voucher_type_id','=',$type_id)
                                        ->whereNull('user_id');
        if($voucher_normal->count() > 0){

            if($status == 'to_used') $voucher_normal->update(array('used' => true));
            if($status == 'to_unused') $voucher_normal->update(array('used' => false));

        }
    }

    public function delete($id)
    {

        if(isset($id)){

            try{
                $user = VoucherNormalType::findOrFail($id);
                $user->delete();

                Session::flash('success','Type deleted');

                return redirect()->back();

            }catch(Exception $e) {
                return Session::flash('error','Something wrong');
            }

        }

    }
}
