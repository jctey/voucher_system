<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\VoucherNewuser;
use App\Models\VoucherNewuserType;
use Carbon\Carbon;
use Session;

class VoucherNewuserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('adminCheck');
    }

    public function index()
    {
        return view('admin.voucher_newuser.index')
            ->with('newuser_type',VoucherNewuserType::all());
    }

    public function listDatatable(Request $req)
    {
        $vouchers = VoucherNewuser::all();

        if($req->newuser_used){

            $newuser_used_status = $req->newuser_used;

            if($newuser_used_status == 'n-used'){
                $vouchers= $vouchers->where('used', '=', 1);
            }else{
                $vouchers= $vouchers->where('used', '=', 0);
            }

        }

        if($req->newuser_type){

            $newuser_type_id = $req->newuser_type;
            $vouchers = $vouchers->where('voucher_type_id','=',$newuser_type_id);
        }

        return Datatables::of($vouchers)
            ->addIndexColumn()
            ->addColumn('action', function ($user) {
                return '<div style="display:flex; justify-content: flex-start;"><a href="'.route('voucher-newuser-edit',$user->id).'" class="edit btn btn-info btn-sm shadow"> Edit</a><button data-toggle="modal" data-target="#confirmModal" data-modaltype="newuser" data-userid='.$user->id.' class="ml-2 delete btn btn-danger btn-sm shadow">Delete</button></div>';
            })

            ->editColumn('id', '{{$id}}')
            ->editColumn('user_id', function ($user) {

                if(empty($user->user->email)){
                    return '';
                }else{
                    return $user->user->email;
                }
            })
            ->editColumn('used', function ($user) {
                if($user->used == 1) return 'used';
                else return 'unused';
            })
            ->editColumn('created_at', function ($user) {
                $formatedDate = Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->format('d/m/Y G:i:s A');
                return $formatedDate;
            })

            ->rawColumns(['action'])
            ->removeColumn('updated_at')
            ->setRowId('id')
            ->setRowClass(function ($user) {
                return $user->used == 1 ? 'alert-info' : 'alert-secondary';
            })

            ->make(true);
    }

    public function edit($id)
    {
        return view('admin.voucher_newuser.edit')
        ->with('newuser',VoucherNewuser::findOrFail($id));
    }

    public function update(Request $request ,$id)
    {
        $voucher_newuser = VoucherNewuser::findOrFail($id);

        if($request->visible) $visible = true;
        else $visible = false;

        $voucher_newuser->visible = $visible;

        $voucher_newuser->save();

        Session::flash('success','Updated successfully');

        return redirect(route('voucher-newuser-index'));;
    }


    public function delete($id)
    {
        if(isset($id)){

            try{
                $user = VoucherNewuser::findOrFail($id);
                $user->delete();

                Session::flash('success','Record deleted');

                return redirect()->back();

            }catch(Exception $e) {
                return Session::flash('error','Something wrong');
            }

        }

    }
}
