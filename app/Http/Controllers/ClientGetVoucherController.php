<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\VoucherLimitedType;
use App\Models\VoucherLimited;
use App\Models\VoucherNormalType;
use App\Models\VoucherNormal;
use Session;
use App\Jobs\GetVoucher;

use Illuminate\Support\Facades\Auth;

class ClientGetVoucherController extends Controller
{

    public function get_voucher(Request $req)
    {
        $voucher_type = $req->voucher_type;
        $voucher_type_id = $req->voucher_type_id;
        $user_id = Auth::user()->id;

        GetVoucher::dispatch($voucher_type,$voucher_type_id,$user_id);

        return redirect()->back();

        // if($voucher_type){
        //     // voucher_type :   limited , normal

        //     if($voucher_type == 'normal'){

        //         if($voucher_type_id){

        //             //check voucher type
        //             if (VoucherNormalType::where('id', '=', $voucher_type_id)->exists()) {

        //                 if (VoucherNormal::where('voucher_type_id',$voucher_type_id)->where('user_id', '=', Auth::user()->id)->exists()) {
        //                     Session::flash('error','You get this voucher before');
        //                 }

        //                 $this->check_update_voucher_normal($voucher_type_id);
        //                 return redirect()->back();

        //             }else {
        //                 Session::flash('error','Voucher doesnt exist');
        //                 redirect()->back();
        //             }

        //         }else {
        //             Session::flash('error','No voucher id provided');
        //             redirect()->back();
        //         }

        //     }else if($voucher_type == 'limited'){

        //         if($voucher_type_id){

        //             //check voucher type
        //             if (VoucherLimitedType::where('id', '=', $voucher_type_id)->exists()) {

        //                 if (VoucherLimited::where('voucher_type_id',$voucher_type_id)->where('user_id', '=', Auth::user()->id)->exists()) {
        //                     Session::flash('error','You get this voucher before');
        //                 }

        //                 $this->check_update_voucher_limited($voucher_type_id);
        //                 return redirect()->back();

        //             }else{
        //                 Session::flash('error','Voucher doesnt exist');
        //                 redirect()->back();
        //             }

        //         }else {
        //             Session::flash('error','No Voucher is fully redeemid provided');
        //             redirect()->back();
        //         }

        //     }else {
        //         Session::flash('error','No voucher type provided');
        //         redirect()->back();
        //     }

        // }

    }

    // public function check_update_voucher_limited($voucher_type_id)
    // {
    //     $voucher_limited = VoucherLimited::where('voucher_type_id',$voucher_type_id)->where('user_id',null)->first();

    //     if (is_null($voucher_limited)) {
    //         Session::flash('error','Voucher is fully redeem');
    //     }else{
    //         //update voucher
    //         $voucher_limited->user_id = Auth::user()->id;

    //         $voucher_limited->save();

    //         Session::flash('success','Successfully get a voucher');
    //     }

    //     //dd($client_voucher_code = $voucher_limited->voucher_code);
    // }

    // public function check_update_voucher_normal($voucher_type_id)
    // {
    //     $voucher_normal = VoucherNormal::where('voucher_type_id',$voucher_type_id)->where('user_id',null)->first();

    //     if (is_null($voucher_normal)) {
    //         Session::flash('error','Voucher is fully redeem');
    //     }else{
    //         //update voucher

    //         $voucher_normal->user_id = Auth::user()->id;

    //         $voucher_normal->save();

    //         Session::flash('success','Successfully get a voucher');
    //     }

    //     //dd($client_voucher_code = $voucher_limited->voucher_code);
    // }
}
