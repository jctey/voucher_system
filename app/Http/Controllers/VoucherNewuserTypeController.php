<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\VoucherNewuserType;
use App\Models\VoucherNewuser;
use App\Http\Requests\newuserType\createNewuserTypeRequest;
use App\Http\Requests\newuserType\updateNewuserTypeRequest;
use Session;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class VoucherNewuserTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('adminCheck');
    }

    public function create()
    {
        return view('admin.voucher_newuser_type.edit');
    }

    public function store(createNewuserTypeRequest $request)
    {

        if($request->hasFile('image')){

            $image = $request->image;
            $imageName = time().'.'.$request->image->getClientOriginalName();
            $image->move(public_path('uploads/voucher_newuser_type'), $imageName);
        }else{
            $imageName = 'default.jpeg';
        }

        $show = 0;
        $description = '';

        if($request->show) $show = $request->show;
        if($request->description) $description = $request->description;

        //create voucher type
        $voucher_newuser_type = VoucherNewuserType::create([
            'type_name' => $request->type_name,
            'show' => $show,
            'description' => $description,
            'image' => 'uploads/voucher_newuser_type/'.$imageName,
        ]);

        Session::flash('success','New type created successfully');

        return redirect(route('voucher-newuser-index'));
    }

    public function listDatatable()
    {
        $voucher_types = VoucherNewuserType::all();

        return Datatables::of($voucher_types)
            ->addIndexColumn()
            ->addColumn('action', function ($user) {
                return '<div style="display:flex; justify-content: flex-start;"><a href="'.route('voucher-newuser-type-edit',$user->id).'" class="edit btn btn-info btn-sm shadow"> Edit</a><button data-toggle="modal" data-target="#confirmModal" data-modaltype="newuser_type" data-typeid='.$user->id.' class="ml-2 delete btn btn-danger btn-sm shadow">Delete</button></div>';
            })

            ->editColumn('id', '{{$id}}')

            ->rawColumns(['action'])
            ->setRowId('id')
            ->setRowClass(function ($user) {
                return 'alert-secondary';
            })

            ->make(true);
    }

    public function edit($id)
    {
        return view('admin.voucher_newuser_type.edit')
                ->with('voucher_type',VoucherNewuserType::findOrFail($id));;
    }

    public function update(updateNewuserTypeRequest $request, $id)
    {
        $voucher_newuser_type = VoucherNewuserType::findOrFail($id);

        if($request->hasFile('image')){

            $image = $request->image;
            $imageName = time().'.'.$request->image->getClientOriginalName();
            $image->move(public_path('uploads/voucher_newuser_type'), $imageName);
            $voucher_newuser_type->image = 'uploads/voucher_newuser_type/'.$imageName;
        }

        $show = 0;

        if($request->show) $show = $request->show;

        $voucher_newuser_type->type_name = $request->type_name;
        $voucher_newuser_type->show = $show;
        $voucher_newuser_type->description = $request->description;

        $voucher_newuser_type->save();

        Session::flash('success','Type updated successfully');

        return redirect(route('voucher-newuser-index'));;
    }

    public function delete($id)
    {
        if(isset($id)){
            try{
                $user = VoucherNewuserType::findOrFail($id);
                $user->delete();

                Session::flash('success','Type deleted');

                return redirect()->back();

            }catch(Exception $e) {
                return Session::flash('error','Something wrong');
            }

        }

    }
}
