<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\VoucherBirthday;
use App\Models\VoucherBirthdayType;
use Carbon\Carbon;
use Session;

class VoucherBirthdayController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('adminCheck');
    }

    public function index()
    {
        return view('admin.voucher_birthday.index')
            ->with('birthday_type',VoucherBirthdayType::all());
    }

    public function listDatatable(Request $req)
    {
        $vouchers = VoucherBirthday::all();

        if($req->birthday_used){

            $birthday_used_status = $req->birthday_used;

            if($birthday_used_status == 'b-used'){
                $vouchers= $vouchers->where('used', '=', 1);
            }else{
                $vouchers= $vouchers->where('used', '=', 0);
            }

        }

        if($req->birthday_type){

            $birthday_type_id = $req->birthday_type;
            $vouchers = $vouchers->where('voucher_type_id','=',$birthday_type_id);
        }

        return Datatables::of($vouchers)
            ->addIndexColumn()
            ->addColumn('action', function ($user) {
                return '<div style="display:flex; justify-content: flex-start;"><a href="'.route('voucher-birthday-edit',$user->id).'" class="edit btn btn-info btn-sm shadow"> Edit</a><button data-toggle="modal" data-target="#confirmModal" data-modaltype="birthday" data-userid='.$user->id.' class="ml-2 delete btn btn-danger btn-sm shadow">Delete</button></div>';
            })

            ->editColumn('id', '{{$id}}')
            ->editColumn('user_id', function ($user) {

                if(empty($user->user->email)){
                    return '';
                }else{
                    return $user->user->email;
                }
            })
            ->editColumn('used', function ($user) {
                if($user->used == 1) return 'used';
                else return 'unused';
            })
            ->editColumn('created_at', function ($user) {
                $formatedDate = Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->format('d/m/Y G:i:s A');
                return $formatedDate;
            })

            ->rawColumns(['action'])
            ->removeColumn('updated_at')
            ->setRowId('id')
            ->setRowClass(function ($user) {
                return $user->used == 1 ? 'alert-info' : 'alert-secondary';
            })

            ->make(true);
    }

    public function edit($id)
    {
        return view('admin.voucher_birthday.edit')
        ->with('birthday',VoucherBirthday::findOrFail($id));
    }

    public function update(Request $request ,$id)
    {
        $voucher_birthday = VoucherBirthday::findOrFail($id);

        if($request->visible) $visible = true;
        else $visible = false;

        $voucher_birthday->visible = $visible;

        $voucher_birthday->save();

        Session::flash('success','Updated successfully');

        return redirect(route('voucher-birthday-index'));;
    }

    public function delete($id)
    {
        if(isset($id)){

            try{
                $user = VoucherBirthday::findOrFail($id);
                $user->delete();

                Session::flash('success','Record deleted');

                return redirect()->back();

            }catch(Exception $e) {
                return Session::flash('error','Something wrong');
            }

        }

    }
}
