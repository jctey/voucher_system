<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\VoucherNormal;
use App\Models\VoucherNormalType;
use Carbon\Carbon;
use Session;

class VoucherNormalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('adminCheck');
    }

    public function index()
    {
        return view('admin.voucher_normal.index')
                    ->with('normal_type',VoucherNormalType::all());
    }

    public function listDatatable(Request $req)
    {
        $vouchers = VoucherNormal::all();

        if($req->normal_used){

            $normal_used_status = $req->normal_used;

            if($normal_used_status == 'n-used'){
                $vouchers= $vouchers->where('used', '=', 1);
            }else{
                $vouchers= $vouchers->where('used', '=', 0);
            }

        }

        if($req->normal_type){

            $normal_type_id = $req->normal_type;
            $vouchers = $vouchers->where('voucher_type_id','=',$normal_type_id);
        }

        return Datatables::of($vouchers)
            ->addIndexColumn()
            ->addColumn('action', function ($user) {
                return '<div style="display:flex; justify-content: flex-start;"><a href="'.route('voucher-normal-edit',$user->id).'" class="edit btn btn-info btn-sm shadow"> Edit</a><button data-toggle="modal" data-target="#confirmModal" data-modaltype="normal" data-userid='.$user->id.' class="ml-2 delete btn btn-danger btn-sm shadow">Delete</button></div>';
            })

            ->editColumn('id', '{{$id}}')
            ->editColumn('user_id', function ($user) {

                if(empty($user->user->email)){
                    return '';
                }else{
                    return $user->user->email;
                }
            })
            ->editColumn('used', function ($user) {
                if($user->used == 1) return 'used';
                else return 'unused';
            })
            ->editColumn('created_at', function ($user) {
                $formatedDate = Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->format('d/m/Y G:i:s A');
                return $formatedDate;
            })

            ->rawColumns(['action'])
            ->removeColumn('updated_at')
            ->setRowId('id')
            ->setRowClass(function ($user) {
                return $user->used == 1 ? 'alert-info' : 'alert-secondary';
            })

            ->make(true);
    }

    public function edit($id)
    {
        return view('admin.voucher_normal.edit')
        ->with('normal',VoucherNormal::findOrFail($id));
    }

    public function update(Request $request ,$id)
    {
        $voucher_normal = VoucherNormal::findOrFail($id);

        if($request->visible) $visible = true;
        else $visible = false;

        $voucher_normal->visible = $visible;

        $voucher_normal->save();

        Session::flash('success','Updated successfully');

        return redirect(route('voucher-normal-index'));;
    }


    public function delete($id)
    {

        if(isset($id)){

            try{
                $user = VoucherNormal::findOrFail($id);
                $user->delete();

                Session::flash('success','Record deleted');

                return redirect()->back();

            }catch(Exception $e) {
                return Session::flash('error','Something wrong');
            }

        }

    }
}
