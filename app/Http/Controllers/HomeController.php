<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Helpers\PaginationHelper;
use DataTables;
use App\Models\User;
use App\Models\VoucherNewuser;
use App\Models\VoucherBirthday;
use App\Models\VoucherLimited;
use App\Models\VoucherNormal;
use App\Models\VoucherLimitedType;
use App\Models\VoucherNormalType;
use App\Models\VoucherType;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;
use Session;
use App\Http\Requests\user\createUserRequest;
use App\Http\Requests\user\updateUserRequest;
use Illuminate\Pagination\LengthAwarePaginator;
class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(Auth::check() && auth()->user()->isAdmin()){

            return redirect()->route('dashboard');

        }else {
            $user_id = auth()->user()->id;

            // $vl = tap(VoucherLimitedType::where('show','=',1)

            //     ->where('unpublish_date', '>=', date('Y-m-d H:i:s'))
            //     ->paginate(12,['*'],'limit-voucher-list'),function($paginatedInstance){
            //         return $paginatedInstance->getCollection()->transform(function ($data) {
            //             $data->type = "limited";
            //             if(($data->publish_date <= date('Y-m-d H:i:s')) && ($data->unpublish_date >= date('Y-m-d H:i:s')) && !(Auth::user()->checkHaveLimitedVoucher($data->id)) && (Auth::user()->CheckHaveLimitedVoucherAvailable($data->id))){
            //                 $data->status = "available";
            //                 $data->sequence = 1;

            //             }else if($data->publish_date > date('Y-m-d H:i:s')){
            //                 $data->status = "coming soon";
            //                 $data->sequence = 2;

            //             }else if(!Auth::user()->CheckHaveLimitedVoucherAvailable($data->id)){
            //                 $data->status = "fully redeem";
            //                 $data->sequence = 3;

            //             }else {
            //                 $data->status = "not available";
            //                 $data->sequence = 4;
            //             }
            //             return $data;
            //         });
            // });
            $vl = VoucherLimitedType::where('show','=',1)
                    ->where('unpublish_date', '>=', date('Y-m-d H:i:s'))
                    ->get();
            $vl = $vl->map(function ($data, $key){
                $data->type = "limited";

                if(($data->publish_date <= date('Y-m-d H:i:s')) && ($data->unpublish_date >= date('Y-m-d H:i:s')) && !(Auth::user()->checkHaveLimitedVoucher($data->id)) && (Auth::user()->CheckHaveLimitedVoucherAvailable($data->id))){
                    $data->status = "available";
                    $data->sequence = 1;

                }else if($data->publish_date > date('Y-m-d H:i:s')){
                    $data->status = "coming soon";
                    $data->sequence = 2;

                }else if(!Auth::user()->CheckHaveLimitedVoucherAvailable($data->id)){
                    $data->status = "fully redeem";
                    $data->sequence = 3;

                }else {
                    $data->status = "not available";
                    $data->sequence = 4;
                }
                return $data;
            });

            // $vn = tap(VoucherNormalType::where('show','=',1)
            //     ->paginate(12,['*'],'normal-voucher-list'),function($paginatedInstance){
            //         return $paginatedInstance->getCollection()->transform(function ($data) {
            //             $data->type = "normal";
            //             if(!(Auth::user()->checkHaveNormalVoucher($data->id)) && (Auth::user()->checkHaveNormalVoucherAvailable($data->id))){
            //                 $data->status = "available";
            //                 $data->sequence = 1;
            //             }else if(!Auth::user()->checkHaveNormalVoucherAvailable($data->id)){
            //                 $data->status = "fully redeem";
            //                 $data->sequence = 2;
            //             }else {
            //                 $data->status = "not available";
            //                 $data->sequence = 3;
            //             }
            //             return $data;
            //         });
            // });

            $vn = VoucherNormalType::where('show','=',1)->get();
            $vn = $vn->map(function ($data, $key){
                $data->type = "normal";
                if(VoucherNormal::existVoucherOf($data->id)) return;

                if(!(Auth::user()->checkHaveNormalVoucher($data->id)) && (Auth::user()->checkHaveNormalVoucherAvailable($data->id))){
                    $data->status = "available";
                    $data->sequence = 1;
                }else if(!Auth::user()->checkHaveNormalVoucherAvailable($data->id)){
                    $data->status = "fully redeem";
                    $data->sequence = 2;
                }else {
                    $data->status = "not available";
                    $data->sequence = 3;
                }
                return $data;
            });

            // filter collection null
            $vl = $vl->filter(function ($value) { return !is_null($value); });
            $vn = $vn->filter(function ($value) { return !is_null($value); });

            $vl = $vl->sortBy('sequence');
            $vn = $vn->sortBy('sequence');
            // $sortedResultLimited = $vl->getCollection()->sortBy('sequence');
            // $vl = $vl->setCollection($sortedResultLimited);

            // $sortedResultNormal = $vn->getCollection()->sortBy('sequence');
            // $vn = $vn->setCollection($sortedResultNormal);

            $showPerPage = 12;
            $vl = PaginationHelper::paginate($vl, $showPerPage,'voucher-list-limited');
            $vn = PaginationHelper::paginate($vn, $showPerPage,'voucher-list-normal');
            $view = view('home')
                ->with('voucher_list_limited',$vl)
                ->with('voucher_list_normal',$vn);

            return $view;
            //return response($view)->header('Cache-Control', 'no-cache, must-revalidate');
        }
    }

    public function my_voucher()
    {
        if((Auth::check()) && !(auth()->user()->isAdmin())){

            $user = auth()->user();

            // show
            // 0 - not show
            // 1 - show all, hide all (home, myvoucher, history)
            // 2 - show at (myvoucher, history)
            // 3 - show at (history)

            $voucherr = $user->voucherNewuser()->where('visible',1)->where('used',0)->get();
            $voucherr = $voucherr->map(function ($data, $key){
                $data->voucher_type_id = $data->voucherNewuserType->type_name;
                $data->image = $data->voucherNewuserType->image;
                $data->type = "newuser";
                $data->show = $data->voucherNewuserType->show;
                return $data;
            });
            $voucherb =[];
            if(auth()->user()->ShowBeforeOneWeek()){
                $voucherb = $user->voucherBirthday()->where('visible',1)->where('used',0)->get();
                $voucherb = $voucherb->map(function ($data, $key){
                    $data->voucher_type_id = $data->voucherBirthdayType->type_name;
                    $data->image = $data->voucherBirthdayType->image;
                    $data->type = "birthday";
                    $data->show =$data->voucherBirthdayType->show;
                    return $data;
                });
            }

            $voucherl = $user->voucherLimited()->where('visible',1)->where('used',0)->get();
            $voucherl = $voucherl->map(function ($data, $key){
                $data->voucher_type_id = $data->voucherLimitedType->type_name;
                $data->image = $data->voucherLimitedType->image;
                $data->type = "limited";
                $data->show =$data->voucherLimitedType->show;
                return $data;
            });

            $vouchern = $user->voucherNormal()->where('visible',1)->where('used',0)->get();
            $vouchern = $vouchern->map(function ($data, $key){
                $data->voucher_type_id = $data->voucherNormalType->type_name;
                $data->image = $data->voucherNormalType->image;
                $data->type = "normal";
                $data->show =$data->voucherNormalType->show;
                return $data;
            });

            $voucher = $voucherr->concat($voucherb);
            $voucher = $voucher->concat($voucherl);
            $voucher = $voucher->concat($vouchern);
            $voucher = $voucher->sortByDesc('updated_at');

            $showPerPage = 6;
            $voucher = PaginationHelper::paginate($voucher, $showPerPage);

            $view =  view('my_voucher')
                    ->with('vouchers',$voucher);

            return response($view)->header('Cache-Control', 'no-cache, must-revalidate');


        }else return view('home');
    }

    public function my_voucher_detail(Request $request)
    {
        if((Auth::check()) && !(auth()->user()->isAdmin())){

            if($request->type){
                if($request->type == 'newuser') {
                    if($request->type_id) {
                        $details = auth()->user()->voucherNewuser()->where('id','=',$request->type_id)->first();

                        if($details->used == true) return redirect()->route('my-voucher');

                        $details->image = $details->voucherNewuserType->image;

                        $time_left = $this->countDownTimer($details->updated_at);

                        if(($time_left == false) && ($details->used == false) && ($details->status =='active')){
                            unset($details->image);
                            $details->status = 'inactive';
                            $details->used = true;
                            $details->save();
                            $time_left = 'Invalid date';
                        }
                        $type_id = $request->type_id;

                        if($details->status == 'inactive') $time_left = '';
                    }

                    if($details->status == 'inactive') $time_left = '';
                }
                if($request->type == 'birthday') {
                    if($request->type_id) {
                        $details = auth()->user()->voucherBirthday()->where('id','=',$request->type_id)->first();

                        if($details->used == true) return redirect()->route('my-voucher');

                        $details->image = $details->voucherBirthdayType->image;

                        $time_left = $this->countDownTimer($details->updated_at);

                        if(($time_left == false) && ($details->used == false) && ($details->status =='active')){
                            unset($details->image);
                            $details->status = 'inactive';
                            $details->used = true;
                            $details->save();
                            $time_left = 'Invalid date';
                        }
                        $type_id = $request->type_id;

                        if($details->status == 'inactive') $time_left = '';
                    }

                }
                if($request->type == 'limited'){
                    if($request->type_id) {
                        $details = auth()->user()->voucherLimited()->where('id','=',$request->type_id)->first();

                        if($details->used == true) return redirect()->route('my-voucher');

                        $details->image = $details->voucherLimitedType->image;

                        $time_left = $this->countDownTimer($details->updated_at);

                        if(($time_left == false) && ($details->used == false) && ($details->status =='active')){
                            unset($details->image);
                            $details->status = 'inactive';
                            $details->used = true;
                            $details->save();
                            $time_left = 'Invalid date';
                        }
                        $type_id = $request->type_id;

                        if($details->status == 'inactive') $time_left = '';
                    }
                }
                if($request->type == 'normal'){
                    if($request->type_id) {
                        $details = auth()->user()->voucherNormal()->where('id','=',$request->type_id)->first();

                        if($details->used == true) return redirect()->route('my-voucher');

                        $details->image = $details->voucherNormalType->image;

                        $time_left = $this->countDownTimer($details->updated_at);

                        if(($time_left == false) && ($details->used == false) && ($details->status =='active')){
                            unset($details->image);
                            $details->status = 'inactive';
                            $details->used = true;
                            $details->save();
                            $time_left = 'Invalid date';
                        }
                        $type_id = $request->type_id;

                        if($details->status == 'inactive') $time_left = '';
                    }
                }

            }else return redirect()->back();

            return view('my_voucher_detail')
                    ->with('details',$details)
                    ->with('voucher_type',$request->type)
                    ->with('type_id',$type_id)
                    ->with('time_left',$time_left);

        }else return redirect()->back();

    }

    public function activate_voucher(Request $request)
    {
        if($request->voucher_type) {
            if($request->voucher_type == 'newuser'){

                $update = auth()->user()->voucherNewuser()->where('id','=',$request->type_id)->first();

                if($update){
                    $update->status = 'active';
                    $update->save();
                    return response()->json(['msg' => 'success']);

                }else return redirect()->back();
            }
            if($request->voucher_type == 'birthday') {

                if(date('d-m', strtotime(auth()->user()->date_of_birth)) == date('d-m')){
                    $update = auth()->user()->voucherBirthday()->first();

                    if($update){
                        $update->status = 'active';
                        $update->save();
                        return response()->json(['msg' => 'success']);

                    }else return redirect()->back();
                }else return redirect()->back();

            }
            if($request->voucher_type == 'limited'){
                if($request->type_id) {

                    $update = auth()->user()->voucherLimited()->where('id','=',$request->type_id)->first();

                    if($update){
                        $update->status = 'active';
                        $update->save();

                        return response()->json(['msg' => 'success']);
                    }else return redirect()->back();
                }
            }
            if($request->voucher_type == 'normal'){
                if($request->type_id) {

                    $update = auth()->user()->voucherNormal()->where('id','=',$request->type_id)->first();

                    if($update){
                        $update->status = 'active';
                        $update->save();

                        return response()->json(['msg' => 'success']);
                    }else return redirect()->back();
                }
            }
        }

    }

    public function countDownTimer($startTime)
    {
        $startTimeDate = strtotime($startTime);
        $currentDate = strtotime(Carbon::now());

        $timesUpDateS = date("Y-m-d H:i:s", $startTimeDate+(60*5)); //add 5 minutes

        //check date between start and end

            if(($startTime <= Carbon::now()) && (Carbon::now() <= $timesUpDateS)){

                $timesUpDate = strtotime($timesUpDateS);

                // Formulate the Difference between two dates
                $diff = abs($timesUpDate - $currentDate);

                // To get the year divide the resultant date into
                // total seconds in a year (365*60*60*24)
                $years = floor($diff / (365*60*60*24));


                // To get the month, subtract it with years and
                // divide the resultant date into
                // total seconds in a month (30*60*60*24)
                $months = floor(($diff - $years * 365*60*60*24)
                                            / (30*60*60*24));


                // To get the day, subtract it with years and
                // months and divide the resultant date into
                // total seconds in a days (60*60*24)
                $days = floor(($diff - $years * 365*60*60*24 -
                            $months*30*60*60*24)/ (60*60*24));


                // To get the hour, subtract it with years,
                // months & seconds and divide the resultant
                // date into total seconds in a hours (60*60)
                $hours = floor(($diff - $years * 365*60*60*24
                    - $months*30*60*60*24 - $days*60*60*24)
                                                / (60*60));


                // To get the minutes, subtract it with years,
                // months, seconds and hours and divide the
                // resultant date into total seconds i.e. 60
                $minutes = floor(($diff - $years * 365*60*60*24
                        - $months*30*60*60*24 - $days*60*60*24
                                        - $hours*60*60)/ 60);


                // To get the minutes, subtract it with years,
                // months, seconds, hours and minutes
                $seconds = floor(($diff - $years * 365*60*60*24
                        - $months*30*60*60*24 - $days*60*60*24
                                - $hours*60*60 - $minutes*60));

                return $minutes.':'.$seconds;
                // return $years.'years'.$months.'months'.$days.'days'.$hours.'hours'.$minutes.'minutes'.$seconds.'seconds';
            }else return false;

    }

    public function history()
    {
        if((Auth::check()) && !(auth()->user()->isAdmin())){

            $user = auth()->user();

            $voucherr = $user->voucherNewuser()->where('visible',1)->where('used',1)->get();
            $voucherr = $voucherr->map(function ($data, $key){
                $data->voucher_type_id = $data->voucherNewuserType->type_name;
                $data->image = $data->voucherNewuserType->image;
                $data->type = "newuser";
                $data->show = $data->voucherNewuserType->show;
                return $data;
            });

            $voucherb = $user->voucherBirthday()->where('visible',1)->where('used',1)->get();
            $voucherb = $voucherb->map(function ($data, $key){
                $data->voucher_type_id = $data->voucherBirthdayType->type_name;
                $data->image = $data->voucherBirthdayType->image;
                $data->type = "birthday";
                $data->show =$data->voucherBirthdayType->show;
                return $data;
            });

            $voucherl = $user->voucherLimited()->where('visible',1)->where('used',1)->get();
            $voucherl = $voucherl->map(function ($data, $key){
                $data->voucher_type_id = $data->voucherLimitedType->type_name;
                $data->image = $data->voucherLimitedType->image;
                $data->type = "limited";
                $data->show =$data->voucherLimitedType->show;
                return $data;
            });

            $vouchern = $user->voucherNormal()->where('visible',1)->where('used',1)->get();
            $vouchern = $vouchern->map(function ($data, $key){
                $data->voucher_type_id = $data->voucherNormalType->type_name;
                $data->image = $data->voucherNormalType->image;
                $data->type = "normal";
                $data->show =$data->voucherNormalType->show;
                return $data;
            });

            $histories = $voucherr->concat($voucherb);
            $histories = $histories->concat($voucherl);
            $histories = $histories->concat($vouchern);

            if(count($histories)>0) $histories = $histories->sortByDesc('updated_at');

            $showPerPage = 12;
            $histories = PaginationHelper::paginate($histories, $showPerPage);

            return view('history')->with('histories',$histories);

        }else return view('home');
    }

    public function userIndex()
    {
        return view('admin.users.index');
    }

    public function userListDatatable(Request $req)
    {
        $users = User::select(['id', 'is_admin','name', 'phone_number', 'email', 'date_of_birth', 'email_verified_at', 'created_at']);

        if($req->verify){
            if($req->verify == 'verify') $users = $users->whereNotNull('email_verified_at');
            else $users = $users->WhereNull('email_verified_at');
        }
        if($req->role){
            if($req->role == 'admin') $users = $users->where('is_admin','=',true);
            if($req->role == 'user') $users = $users->where('is_admin','=',false);
        }

        return Datatables::of($users)
            ->addIndexColumn()
            ->addColumn('action', function ($user) {
                if($user->id != 1){
                    return '<div style="display:flex; justify-content: space-evenly;"><a href="'.route('user-edit',$user->id).'" class="edit btn btn-info btn-sm shadow"> Edit</a><button data-toggle="modal" data-target="#confirmModal" data-modaltype="user" data-useremail='.$user->email.' data-userid='.$user->id.' class="delete btn btn-danger btn-sm shadow">Delete</button></div';
                }
            })

            ->editColumn('id', '{{$id}}')
            ->editColumn('email_verified_at', function ($user) {
                if(empty($user->email_verified_at)){
                    return '<span class="badge badge-pill badge-warning">Unverified</span>';
                }else{
                    return '<span class="badge badge-pill badge-success">Verified</span>';
                }
            })
            ->editColumn('created_at', function ($user) {
                $formatedDate = Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->format('d/m/Y G:i:s A');
                return $formatedDate;
            })

            ->rawColumns(['email_verified_at','action'])
            ->removeColumn('updated_at')
            ->setRowId('id')
            ->setRowClass(function ($user) {
                return $user->isAdmin() ? 'alert-info' : 'alert-secondary';
            })

            ->make(true);
    }

    public function userEdit($id)
    {
        return view('admin.users.edit')
                ->with('user',User::findOrFail($id));
    }

    public function userUpdate(updateUserRequest $request, $id)
    {
        $user = User::findOrFail($id);

        $user->name = $request->name;
        $user->email = $request->email;

        if($request->phone_number) $user->phone_number = $request->phone_number;
        if($request->date_of_birth) $user->date_of_birth = $request->date_of_birth;
        if($request->password) $user->password = Hash::make($request->password);

        $user->save();

        Session::flash('success','User updated successfully');

        return redirect()->route('user-index');
    }

    public function userDelete($id)
    {
        if($id == 1){
            return Session::flash('error','Cannot delete admin');
        }else{
            try{
                $user = User::findOrFail($id);
                $user->delete();

                Session::flash('success','User delete successfully');

                return redirect()->back();

            }catch(Exception $e) {
                return Session::flash('error','Something wrong');
            }
        }

    }

    public function userCreate()
    {
        return view('admin.users.create');
    }

    public function userStore(createUserRequest $request)
    {
        $is_admin = false;

        if($request->is_admin) {
            $is_admin = true;

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password'=> Hash::make($request->password),
                'is_admin' => $is_admin
            ]);

            $user->markEmailAsVerified();

        }else{

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email ,
                'phone_number'=> $request->phone_number,
                'date_of_birth'=> $request->date_of_birth,
                'password'=> Hash::make($request->password),
                'is_admin' => $is_admin
            ]);

            if($user){
                //verify email
                if($request->verify == 'verify') $user->markEmailAsVerified();
                //get voucher
                $user->GetVoucher($user);
            }

        }

        Session::flash('success','User Created successfully');

        return redirect()->route('user-index');
    }

    public function dashboard()
    {

        $newuser_list = User::where('is_admin',false)->latest()->take(30)->get();
        $register_v_list = VoucherNewuser::where('used',true)->latest()->take(30)->get();
        $birthday_v_list = VoucherBirthday::where('used',true)->latest()->take(30)->get();
        $limited_v_list = VoucherLimited::where('used',true)->latest()->take(30)->get();
        $normal_v_list = VoucherNormal::where('used',true)->latest()->take(30)->get();

        $period = CarbonPeriod::create(Carbon::now()->subDays(30), Carbon::now());
        $period_day=[];
        $user_data=[];

        $register_data=[];
        // each date within range
        foreach ($period as $date) {

            $period_day[] = $date->format('d-M-Y');

            //user
            $user_data[] = $newuser_list->filter(function ($newuser) use ($date) {
                if($newuser->created_at->format('Y-m-d') == $date->format('Y-m-d')){
                    return $newuser;
                }
            })->count();

            //register v
            $register_data[] = $register_v_list->filter(function ($register_v) use ($date) {
                if($register_v->updated_at->format('Y-m-d') == $date->format('Y-m-d')){
                    return $register_v;
                }
            })->count();

            //birthday v
            $birthday_data[] = $birthday_v_list->filter(function ($birthday_v) use ($date) {
                if($birthday_v->updated_at->format('Y-m-d') == $date->format('Y-m-d')){
                    return $birthday_v;
                }
            })->count();

            //limited v
            $limited_data[] = $limited_v_list->filter(function ($limited_v) use ($date) {
                if($limited_v->updated_at->format('Y-m-d') == $date->format('Y-m-d')){
                    return $limited_v;
                }
            })->count();

            //normal v
            $normal_data[] = $normal_v_list->filter(function ($normal_v) use ($date) {
                if($normal_v->updated_at->format('Y-m-d') == $date->format('Y-m-d')){
                    return $normal_v;
                }
            })->count();

        }

        // Total num
        $total_user = User::where('is_admin',false)->get();
        $total_register_v = VoucherNewuser::where('used',true)->get();
        $total_birthday_v = VoucherBirthday::where('used',true)->get();
        $total_limited_v = VoucherLimited::where('used',true)->get();
        $total_normal_v = Vouchernormal::where('used',true)->get();

        return view('admin.dashboard',[
            'period_day' => $period_day,
            'user_data' => $user_data,

            'register_data' => $register_data,

            'birthday_data' => $birthday_data,

            'limited_data' => $limited_data,

            'normal_data' => $normal_data,

            'total_user' => $total_user->count(),
            'total_register_v' => $total_register_v->count(),
            'total_birthday_v' => $total_birthday_v->count(),
            'total_limited_v' => $total_limited_v->count(),
            'total_normal_v' => $total_normal_v->count(),
        ]);
    }
}
