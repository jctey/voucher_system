<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth','verified']], function(){

    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/my-voucher', [App\Http\Controllers\HomeController::class, 'my_voucher'])->name('my-voucher');
    Route::get('/my-voucher-detail', [App\Http\Controllers\HomeController::class, 'my_voucher_detail'])->name('my-voucher-detail');
    Route::put('/activate-voucher', [App\Http\Controllers\HomeController::class, 'activate_voucher'])->name('activate_voucher');
    Route::get('/history', [App\Http\Controllers\HomeController::class, 'history'])->name('history');

    //api for get vouher
    Route::get('/get-voucher', [App\Http\Controllers\ClientGetVoucherController::class, 'get_voucher'])->name('get-voucher');


    Route::group(['middleware' => ['adminCheck']], function(){

        //user list (newuser & birthday)
        Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('dashboard');
        Route::get('/users', [App\Http\Controllers\HomeController::class, 'userIndex'])->name('user-index');
        Route::get('/user/edit/{id}', [App\Http\Controllers\HomeController::class, 'userEdit'])->name('user-edit');
        Route::post('/user/update/{id}', [App\Http\Controllers\HomeController::class, 'userUpdate'])->name('user-update');
        Route::get('/user/create', [App\Http\Controllers\HomeController::class, 'userCreate'])->name('user-create');
        Route::post('/user/store', [App\Http\Controllers\HomeController::class, 'userStore'])->name('user-store');
        Route::delete('/user/delete/{id}', [App\Http\Controllers\HomeController::class, 'userDelete'])->name('user-delete');

        Route::get('/users-list-datatable', [App\Http\Controllers\HomeController::class, 'userListDatatable'])->name('user-list-datatable');

        //newuser voucher type
        Route::get('/vourcher-newuser-type/create', [App\Http\Controllers\VoucherNewuserTypeController::class, 'create'])->name('voucher-newuser-type-create');
        Route::post('/vourcher-newuser-type/store', [App\Http\Controllers\VoucherNewuserTypeController::class, 'store'])->name('voucher-newuser-type-store');
        Route::get('/vourcher-newuser-type/edit/{id}', [App\Http\Controllers\VoucherNewuserTypeController::class, 'edit'])->name('voucher-newuser-type-edit');
        Route::put('/vourcher-newuser-type/update/{id}', [App\Http\Controllers\VoucherNewuserTypeController::class, 'update'])->name('voucher-newuser-type-update');
        Route::delete('/vourcher-newuser-type/delete/{id}', [App\Http\Controllers\VoucherNewuserTypeController::class, 'delete'])->name('voucher-newuser-type-delete');
        Route::get('/vourchers-newuser-type-list-datatable', [App\Http\Controllers\VoucherNewuserTypeController::class, 'listDatatable'])->name('voucher-newuser-type-list-datatable');

        //newuser voucher
        Route::get('/vourchers-newuser', [App\Http\Controllers\VoucherNewuserController::class, 'index'])->name('voucher-newuser-index');
        Route::get('/vourcher-newuser/edit/{id}', [App\Http\Controllers\VoucherNewuserController::class, 'edit'])->name('voucher-newuser-edit');
        Route::put('/vourcher-newuser/update/{id}', [App\Http\Controllers\VoucherNewuserController::class, 'update'])->name('voucher-newuser-update');
        Route::delete('/vourcher-newuser/delete/{id}', [App\Http\Controllers\VoucherNewuserController::class, 'delete'])->name('voucher-newuser-delete');
        Route::get('/vourchers-newuser-list-datatable', [App\Http\Controllers\VoucherNewuserController::class, 'listDatatable'])->name('voucher-newuser-list-datatable');

        //birthday voucher type
        Route::get('/vourcher-birthday-type/create', [App\Http\Controllers\VoucherBirthdayTypeController::class, 'create'])->name('voucher-birthday-type-create');
        Route::post('/vourcher-birthday-type/store', [App\Http\Controllers\VoucherBirthdayTypeController::class, 'store'])->name('voucher-birthday-type-store');
        Route::get('/vourcher-birthday-type/edit/{id}', [App\Http\Controllers\VoucherBirthdayTypeController::class, 'edit'])->name('voucher-birthday-type-edit');
        Route::put('/vourcher-birthday-type/update/{id}', [App\Http\Controllers\VoucherBirthdayTypeController::class, 'update'])->name('voucher-birthday-type-update');
        Route::delete('/vourcher-birthday-type/delete/{id}', [App\Http\Controllers\VoucherBirthdayTypeController::class, 'delete'])->name('voucher-birthday-type-delete');
        Route::get('/vourchers-birthday-type-list-datatable', [App\Http\Controllers\VoucherBirthdayTypeController::class, 'listDatatable'])->name('voucher-birthday-type-list-datatable');

        //birthday voucher
        Route::get('/vourchers-birthday', [App\Http\Controllers\VoucherBirthdayController::class, 'index'])->name('voucher-birthday-index');
        Route::get('/vourcher-birthday/edit/{id}', [App\Http\Controllers\VoucherBirthdayController::class, 'edit'])->name('voucher-birthday-edit');
        Route::put('/vourcher-birthday/update/{id}', [App\Http\Controllers\VoucherBirthdayController::class, 'update'])->name('voucher-birthday-update');
        Route::delete('/vourcher-birthday/delete/{id}', [App\Http\Controllers\VoucherBirthdayController::class, 'delete'])->name('voucher-birthday-delete');
        Route::get('/vourchers-birthday-list-datatable', [App\Http\Controllers\VoucherBirthdayController::class, 'listDatatable'])->name('voucher-birthday-list-datatable');

        //limited voucher type
        Route::get('/vourcher-limited-type/create', [App\Http\Controllers\VoucherLimitedTypeController::class, 'create'])->name('voucher-limited-type-create');
        Route::post('/vourcher-limited-type/store', [App\Http\Controllers\VoucherLimitedTypeController::class, 'store'])->name('voucher-limited-type-store');
        Route::get('/vourcher-limited-type/edit/{id}', [App\Http\Controllers\VoucherLimitedTypeController::class, 'edit'])->name('voucher-limited-type-edit');
        Route::put('/vourcher-limited-type/update/{id}', [App\Http\Controllers\VoucherLimitedTypeController::class, 'update'])->name('voucher-limited-type-update');
        Route::delete('/vourcher-limited-type/delete/{id}', [App\Http\Controllers\VoucherLimitedTypeController::class, 'delete'])->name('voucher-limited-type-delete');
        Route::get('/vourchers-limited-type-list-datatable', [App\Http\Controllers\VoucherLimitedTypeController::class, 'listDatatable'])->name('voucher-limited-type-list-datatable');

        //limited voucher
        Route::get('/vourchers-limited', [App\Http\Controllers\VoucherLimitedController::class, 'index'])->name('voucher-limited-index');
        Route::get('/vourcher-limited/edit/{id}', [App\Http\Controllers\VoucherLimitedController::class, 'edit'])->name('voucher-limited-edit');
        Route::put('/vourcher-limited/update/{id}', [App\Http\Controllers\VoucherLimitedController::class, 'update'])->name('voucher-limited-update');
        Route::delete('/vourcher-limited/delete/{id}', [App\Http\Controllers\VoucherLimitedController::class, 'delete'])->name('voucher-limited-delete');
        Route::get('/vourchers-limited-list-datatable', [App\Http\Controllers\VoucherLimitedController::class, 'listDatatable'])->name('voucher-limited-list-datatable');

        //normal voucher type
        Route::get('/vourcher-normal-type/create', [App\Http\Controllers\VoucherNormalTypeController::class, 'create'])->name('voucher-normal-type-create');
        Route::post('/vourcher-normal-type/store', [App\Http\Controllers\VoucherNormalTypeController::class, 'store'])->name('voucher-normal-type-store');
        Route::get('/vourcher-normal-type/edit/{id}', [App\Http\Controllers\VoucherNormalTypeController::class, 'edit'])->name('voucher-normal-type-edit');
        Route::put('/vourcher-normal-type/update/{id}', [App\Http\Controllers\VoucherNormalTypeController::class, 'update'])->name('voucher-normal-type-update');
        Route::delete('/vourcher-normal-type/delete/{id}', [App\Http\Controllers\VoucherNormalTypeController::class, 'delete'])->name('voucher-normal-type-delete');
        Route::get('/vourchers-normal-type-list-datatable', [App\Http\Controllers\VoucherNormalTypeController::class, 'listDatatable'])->name('voucher-normal-type-list-datatable');

        //normal voucher
        Route::get('/vourchers-normal', [App\Http\Controllers\VoucherNormalController::class, 'index'])->name('voucher-normal-index');
        Route::get('/vourcher-normal/edit/{id}', [App\Http\Controllers\VoucherNormalController::class, 'edit'])->name('voucher-normal-edit');
        Route::put('/vourcher-normal/update/{id}', [App\Http\Controllers\VoucherNormalController::class, 'update'])->name('voucher-normal-update');
        Route::delete('/vourcher-normal/delete/{id}', [App\Http\Controllers\VoucherNormalController::class, 'delete'])->name('voucher-normal-delete');
        Route::get('/vourchers-normal-list-datatable', [App\Http\Controllers\VoucherNormalController::class, 'listDatatable'])->name('voucher-normal-list-datatable');
    });

});

Route::get('/auth/google/redirect', [App\Http\Controllers\Auth\LoginController::class, 'redirectProviderG'])->name('google-redirect');
Route::get('/auth/google/callback', [App\Http\Controllers\Auth\LoginController::class, 'providerCallbackG'])->name('google-callback');

Route::get('/auth/facebook/redirect', [App\Http\Controllers\Auth\LoginController::class, 'redirectProviderF'])->name('facebook-redirect');
Route::get('/auth/facebook/callback', [App\Http\Controllers\Auth\LoginController::class, 'providerCallbackF'])->name('facebook-callback');



