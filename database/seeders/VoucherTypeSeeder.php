<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VoucherTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('voucher_type')->insert([
            [
                'type_name' => 'New User Voucher',
                'description' => null,
                'image'=>'uploads/voucher_type/newuser/welcome.jpeg'
            ],
            [
                'type_name' => 'Birthday Voucher',
                'description' => null,
                'image'=>'uploads/voucher_type/birthday/birthdaycake.jpg'
            ]
        ]);
    }
}
